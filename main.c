#include <stdlib.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <clutter/clutter.h>

#include "pkg-graph.h"
#include "pkg-scale-linear.h"
#include "pkg-renderer-line.h"

static GOptionEntry options[] = {
	{ NULL }
};

gint
main (gint   argc,
      gchar *argv[])
{
	GOptionContext *context;
	GError *error = NULL;
	ClutterActor *stage,
	             *renderer,
	             *graph,
	             *scale;
	ClutterColor  color;

	/* parse command line arguments */
	context = g_option_context_new(_("- Test App"));
	g_option_context_add_main_entries(context, options, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_printerr("%s\n", error->message);
		g_error_free(error);
		return EXIT_FAILURE;
	}

	/* Initialize libraries */
	g_type_init();
	clutter_init(&argc, &argv);

	/* create graph, scale, and renderer */
	stage = clutter_stage_get_default();
	graph = pkg_graph_new();
	scale = pkg_scale_linear_new();
	renderer = pkg_renderer_line_new();
	pkg_graph_set_scale(PKG_GRAPH(graph), PKG_SCALE(scale));
	pkg_graph_set_renderer(PKG_GRAPH(graph), PKG_RENDERER(renderer));
	pkg_graph_set_padding(PKG_GRAPH(graph), 10, 10);

	color.red = 240;
	color.green = 235;
	color.blue = 226;
	color.alpha = 255;
	clutter_stage_set_color(CLUTTER_STAGE(stage), &color);

	/* add items to stage */
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), graph);
	clutter_actor_set_size(stage, 640, 480);
	clutter_actor_set_size(graph, 640, 480);
	clutter_actor_show_all(stage);

	clutter_main();

	return EXIT_SUCCESS;
}
