/* pkg-scale.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-scale.h"

/**
 * pkg_scale_translate:
 * @scale: A #PkgScale.
 *
 * Translates a coordinate on a given axis into a position matching the
 * the scales size.
 *
 * Returns: None.
 * Side effects: None.
 */
gboolean
pkg_scale_translate (PkgScale *scale,
                     guint     axis,
                     gdouble   in_coord,
                     gdouble  *out_coord)
{
	g_return_val_if_fail(scale != NULL, FALSE);
	g_return_val_if_fail(out_coord != NULL, FALSE);

	return PKG_SCALE_GET_INTERFACE(scale)->
		translate(scale, axis, in_coord, out_coord);
}

GType
pkg_scale_get_type (void)
{
	static GType type_id = 0;

	if (g_once_init_enter((gsize *)&type_id)) {
		GType _type_id;
		const GTypeInfo g_type_info = {
			sizeof(PkgScaleIface),
			NULL, /* base_init */
			NULL, /* base_finalize */
			NULL, /* class_init */
			NULL, /* class_finalize */
			NULL, /* class_data */
			0,    /* instance_size */
			0,    /* n_preallocs */
			NULL, /* instance_init */
			NULL  /* value_vtable */
		};

		_type_id = g_type_register_static(G_TYPE_INTERFACE,
		                                  "PkgScale",
		                                  &g_type_info,
		                                  0);
		g_type_interface_add_prerequisite(_type_id, CLUTTER_TYPE_ACTOR);
		g_once_init_leave((gsize *)&type_id, _type_id);
	}

	return type_id;
}
