all: graph

FILES = main.c pkg-scale.* pkg-scale-linear.* pkg-renderer.* pkg-renderer-line.* pkg-data-set.* pkg-graph.*

graph: $(FILES)
	$(CC) -o $@ -g -Wall -Werror $(FILES) `pkg-config --libs --cflags clutter-1.0` -DPERFKIT_COMPILATION

clean:
	rm -rf graph
