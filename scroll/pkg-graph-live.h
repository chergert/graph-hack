/* pkg-graph-live.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_GRAPH_LIVE_H__
#define __PKG_GRAPH_LIVE_H__

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define PKG_TYPE_GRAPH_LIVE            (pkg_graph_live_get_type())
#define PKG_GRAPH_LIVE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH_LIVE, PkgGraphLive))
#define PKG_GRAPH_LIVE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH_LIVE, PkgGraphLive const))
#define PKG_GRAPH_LIVE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_GRAPH_LIVE, PkgGraphLiveClass))
#define PKG_IS_GRAPH_LIVE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_GRAPH_LIVE))
#define PKG_IS_GRAPH_LIVE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_GRAPH_LIVE))
#define PKG_GRAPH_LIVE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_GRAPH_LIVE, PkgGraphLiveClass))

typedef struct _PkgGraphLive        PkgGraphLive;
typedef struct _PkgGraphLiveClass   PkgGraphLiveClass;
typedef struct _PkgGraphLivePrivate PkgGraphLivePrivate;

struct _PkgGraphLive
{
	ClutterGroup parent;

	/*< private >*/
	PkgGraphLivePrivate *priv;
};

struct _PkgGraphLiveClass
{
	ClutterGroupClass parent_class;
};

GType         pkg_graph_live_get_type     (void) G_GNUC_CONST;
ClutterActor* pkg_graph_live_new          (void);
void          pkg_graph_live_render       (PkgGraphLive *graph);
void          pkg_graph_live_set_range    (PkgGraphLive *graph,
                                           gint          begin,
                                           gint          end);
void          pkg_graph_live_push         (PkgGraphLive *graph,
                                           gdouble       x,
                                           gdouble       y);
gint          pkg_graph_live_count        (PkgGraphLive *graph);

G_END_DECLS

#endif /* __PKG_GRAPH_LIVE_H__ */
