/* pkg-graph-live.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "pkg-graph-live.h"

/**
 * SECTION:pkg-graph-live
 * @title: PkgGraphLive
 * @short_description: 
 *
 * Section overview.
 */

G_DEFINE_TYPE(PkgGraphLive, pkg_graph_live, CLUTTER_TYPE_GROUP)

struct _PkgGraphLivePrivate
{
	ClutterActor *bg_texture;
	ClutterActor *fg_texture;
	gboolean      bg_dirty;
	gboolean      fg_dirty;
	gint          range_begin;
	gint          range_end;
	GArray       *data;
};

typedef struct
{
	gdouble x;
	gdouble y;
} Point;

/**
 * pkg_graph_live_new:
 *
 * Creates a new instance of #PkgGraphLive.
 *
 * Returns: the newly created instance of #PkgGraphLive.
 * Side effects: None.
 */
ClutterActor*
pkg_graph_live_new (void)
{
	return g_object_new(PKG_TYPE_GRAPH_LIVE, NULL);
}

void
pkg_graph_live_set_range (PkgGraphLive *graph,
                          gint          begin,
                          gint          end)
{
	PkgGraphLivePrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH_LIVE(graph));

	priv = graph->priv;

	/*
	 * Ignore requests matching our current range.
	 */
	if (priv->range_begin == begin && priv->range_end == end) {
		return;
	}

	/*
	 * TODO:
	 *
	 *   Match the zoom ratio from the range.  If they match and the data
	 *   overlapps, lets try to slide the data to the new position.
	 */

	/*
	 * Set the visible range.
	 */
	priv->range_begin = begin;
	priv->range_end = end;

	/*
	 * Mark the foreground as dirty.
	 */
	priv->fg_dirty = TRUE;

	/*
	 * Paint the graph.
	 */
	pkg_graph_live_render(graph);
}

static void
pkg_graph_live_render_bg (PkgGraphLive *graph)
{
	PkgGraphLivePrivate *priv;
	ClutterCairoTexture *ct;
	gdouble dashes[] = {1., 2.};
	gfloat w, h;
	cairo_t *cr;

	priv = graph->priv;
	ct = CLUTTER_CAIRO_TEXTURE(priv->bg_texture);

	/*
	 * Get texture size.
	 */
	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);

	/*
	 * Create cairo context.
	 */
	cr = clutter_cairo_texture_create(CLUTTER_CAIRO_TEXTURE(ct));

	/*
	 * Clear the texture.
	 */
	clutter_cairo_texture_clear(ct);

	/*
	 * Paint the background.
	 */
	cairo_set_source_rgb(cr, 1., 1., 1.);
	cairo_rectangle(cr, 0., 0., w, h);
	cairo_fill(cr);

	/*
	 * Paint the grid lines.
	 */
	cairo_set_line_width(cr, 1.);
	cairo_set_dash(cr, dashes, 2, 0);
	cairo_set_source_rgb(cr, 0., 0., 0.);
	cairo_move_to(cr, 0., floor(h / 2) + .5);
	cairo_line_to(cr, w, floor(h / 2) + .5);
	cairo_stroke(cr);

	/*
	 * Cleanup cairo context.
	 */
	cairo_destroy(cr);
}

static void
pkg_graph_live_render_fg (PkgGraphLive *graph)
{
	PkgGraphLivePrivate *priv;
	ClutterCairoTexture *ct;
	gfloat w, h, x_ratio, y_ratio;
	gint begin, end, i;
	cairo_t *cr;

	priv = graph->priv;
	ct = CLUTTER_CAIRO_TEXTURE(priv->fg_texture);

	begin = priv->range_begin;
	if (begin < 0) {
		begin = 0;
	}

	end = priv->range_end;
	if (end >= priv->data->len) {
		end = priv->data->len;
	}

	if ((end <= begin) || ((end - begin) == 0)) {
		clutter_cairo_texture_clear(ct);
		return;
	}

	/*
	 * Clear the drawing area.
	 */
	clutter_cairo_texture_clear(CLUTTER_CAIRO_TEXTURE(ct));

	/*
	 * Get texture size.
	 */
	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);

	/*
	 * Create cairo context.
	 */
	cr = clutter_cairo_texture_create(CLUTTER_CAIRO_TEXTURE(ct));

	/*
	 * Determine the ratio of pixels per array slot.
	 */
	x_ratio = w / (end - begin);
	y_ratio = 0.;

	cairo_set_source_rgb(cr, .4, .4, .4);
	cairo_set_line_width(cr, 1.);

	for (i = begin; i < end; i++) {
		Point *p;

		p = &g_array_index(priv->data, Point, i);
		cairo_line_to(cr,
		              (p->x - begin) * x_ratio,
		              (h / 2.) + (h / 4. * p->y));
	}

	cairo_stroke(cr);

	/*
	 * Cleanup cairo context.
	 */
	cairo_destroy(cr);
}

void
pkg_graph_live_push (PkgGraphLive *graph,
                     gdouble       x,
                     gdouble       y)
{
	Point point;

	g_return_if_fail(PKG_IS_GRAPH_LIVE(graph));

	point.x = x;
	point.y = y;
	g_array_append_val(graph->priv->data, point);
}

void
pkg_graph_live_render (PkgGraphLive *graph)
{
	PkgGraphLivePrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH_LIVE(graph));

	priv = graph->priv;

	if (priv->bg_dirty) {
		pkg_graph_live_render_bg(graph);
		priv->bg_dirty = FALSE;
	}

	if (priv->fg_dirty) {
		pkg_graph_live_render_fg(graph);
		priv->fg_dirty = FALSE;
	}
}

gint
pkg_graph_live_count (PkgGraphLive *graph)
{
	g_return_val_if_fail(PKG_IS_GRAPH_LIVE(graph), 0);
	return graph->priv->data->len;
}

static void
pkg_graph_live_allocation_changed (PkgGraphLive           *graph,
                                   const ClutterActorBox  *box,
                                   ClutterAllocationFlags  flags,
                                   gpointer                user_data)
{
	PkgGraphLivePrivate *priv = graph->priv;
	ClutterCairoTexture *ct;
	gfloat w, h;

	w = clutter_actor_box_get_width(box);
	h = clutter_actor_box_get_height(box);

	clutter_actor_set_size(priv->fg_texture, w, h);
	clutter_actor_set_size(priv->bg_texture, w, h);

	ct = CLUTTER_CAIRO_TEXTURE(priv->fg_texture);
	clutter_cairo_texture_set_surface_size(ct, w, h);

	ct = CLUTTER_CAIRO_TEXTURE(priv->bg_texture);
	clutter_cairo_texture_set_surface_size(ct, w, h);

	priv->bg_dirty = TRUE;
	priv->fg_dirty = TRUE;

	pkg_graph_live_render(graph);
}

static void
pkg_graph_live_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_graph_live_parent_class)->finalize(object);
}

static void
pkg_graph_live_class_init (PkgGraphLiveClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_graph_live_finalize;
	g_type_class_add_private(object_class, sizeof(PkgGraphLivePrivate));
}

static void
pkg_graph_live_init (PkgGraphLive *graph)
{
	PkgGraphLivePrivate *priv;

	priv = graph->priv = G_TYPE_INSTANCE_GET_PRIVATE(graph,
	                                                 PKG_TYPE_GRAPH_LIVE,
	                                                 PkgGraphLivePrivate);

	priv->data = g_array_new(FALSE, FALSE, sizeof(Point));
	priv->fg_texture = clutter_cairo_texture_new(1, 1);
	priv->bg_texture = clutter_cairo_texture_new(1, 1);

	clutter_container_add(CLUTTER_CONTAINER(graph),
	                      priv->bg_texture,
	                      priv->fg_texture,
	                      NULL);

	g_signal_connect(graph,
	                 "allocation-changed",
	                 G_CALLBACK(pkg_graph_live_allocation_changed),
	                 NULL);
}
