#include <math.h>
#include <stdlib.h>
#include <clutter-gtk/clutter-gtk.h>
#include "pkg-graph-live.h"

static GtkWidget *embed = NULL;
static ClutterActor *graph = NULL;

static gdouble
get_page_size (gint zoom)
{
	switch (zoom) {
	case 1:
		return .05;
	case 2:
		return .1;
	case 3:
		return .2;
	case 4:
		return .3;
	case 5:
		return .6;
	case 6:
		return .9;
	case 7:
		return 1.;
	case 8:
		return 1.2;
	default:
		g_assert_not_reached();
	}
}

static void
value_changed (GtkAdjustment *adj,
               PkgGraphLive  *graph)
{
	gdouble value, page_size;
	gint count, begin, end;

	value = gtk_adjustment_get_value(adj);
	page_size = gtk_adjustment_get_page_size(adj);
	count = pkg_graph_live_count(graph);
	begin = value * count;
	end = (value + page_size) * count;

	pkg_graph_live_set_range(graph, begin, end);
}

static void
zoom_value_changed (GtkAdjustment *zadj,
                    GtkAdjustment *adj)
{
	gdouble value;
	gdouble page_size;

	value = gtk_adjustment_get_value(zadj);
	page_size = get_page_size(value);
	gtk_adjustment_set_page_size(adj, page_size);
	value_changed(adj, PKG_GRAPH_LIVE(graph));
}

gint
main (gint   argc,
      gchar *argv[])
{
	GtkWidget *window, *vbox, *scroller, *zoom;
	ClutterActor *stage;
	GtkAdjustment *adj, *zadj;
	ClutterColor bg;
	gint i;

	gtk_clutter_init(&argc, &argv);

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(window), 640, 110);
	gtk_window_set_title(GTK_WINDOW(window), "Side Scrolling");
	gtk_widget_show(window);

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	gtk_widget_show(vbox);

	embed = gtk_clutter_embed_new();
	stage = gtk_clutter_embed_get_stage(GTK_CLUTTER_EMBED(embed));
	gtk_box_pack_start(GTK_BOX(vbox), embed, TRUE, TRUE, 0);
	gtk_widget_show(embed);

	gtk_clutter_get_bg_color(window, GTK_STATE_NORMAL, &bg);
	clutter_stage_set_color(CLUTTER_STAGE(stage), &bg);

	zadj = GTK_ADJUSTMENT(gtk_adjustment_new(3., 1., 10., 1., 2., 2.));
	zoom = gtk_hscale_new(zadj);
	gtk_scale_set_digits(GTK_SCALE(zoom), 0);
	gtk_box_pack_start(GTK_BOX(vbox), zoom, FALSE, TRUE, 0);
	gtk_widget_show(zoom);

	adj = GTK_ADJUSTMENT(gtk_adjustment_new(0., 0., 1., .05, .2, .2));
	scroller = gtk_hscrollbar_new(adj);
	gtk_box_pack_start(GTK_BOX(vbox), scroller, FALSE, TRUE, 0);
	gtk_widget_show(scroller);

	graph = pkg_graph_live_new();
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), graph);
	clutter_actor_set_size(graph, 640, 70);
	clutter_actor_show(graph);

	for (i = 0; i < 1000; i++) {
		pkg_graph_live_push(PKG_GRAPH_LIVE(graph), i, sin(i));
	}

	g_signal_connect(adj,
	                 "value-changed",
	                 G_CALLBACK(value_changed),
	                 graph);

	g_signal_connect(zadj,
	                 "value-changed",
	                 G_CALLBACK(zoom_value_changed),
	                 adj);

	value_changed(adj, PKG_GRAPH_LIVE(graph));

	gtk_main();

	return EXIT_SUCCESS;
}
