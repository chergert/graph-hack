/* pkg-scale-linear.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-scale-linear.h"

#include <math.h>
#include <pango/pangocairo.h>

#define AXIS_X 1
#define AXIS_Y 2
#define LINE_STUB_SIZE (10.5)

/**
 * SECTION:pkg-scale-linear
 * @title: PkgScaleLinear
 * @short_description: 
 *
 * 
 */

static void pkg_scale_init (PkgScaleIface *iface);

G_DEFINE_TYPE_EXTENDED(PkgScaleLinear,
                       pkg_scale_linear,
                       CLUTTER_TYPE_CAIRO_TEXTURE,
                       0,
                       G_IMPLEMENT_INTERFACE(PKG_TYPE_SCALE,
                                             pkg_scale_init))

struct _PkgScaleLinearPrivate
{
	gdouble width;
	gdouble height;

	gdouble begin_x;
	gdouble end_x;
	gdouble begin_y;
	gdouble end_y;

	gdouble label_spacing;
};

/**
 * pkg_scale_linear_new:
 *
 * Creates a new instance of #PkgScaleLinear.
 *
 * Returns: the newly created instance of #PkgScaleLinear.
 * Side effects: None.
 */
ClutterActor*
pkg_scale_linear_new (void)
{
	return g_object_new(PKG_TYPE_SCALE_LINEAR, NULL);
}

static gboolean
pkg_scale_linear_real_translate (PkgScale *scale,
                                 guint     axis,
                                 gdouble   in_coord,
                                 gdouble  *out_coord)
{
	PkgScaleLinearPrivate *priv;
	gdouble ratio;

	priv = ((PkgScaleLinear *)scale)->priv;
	*out_coord = .0;

	/*
	 * If we have not been realized and had our size allocated, then just
	 * go ahead and set the translation to zero.
	 */
	if (!priv->width || !priv->height) {
		return FALSE;
	}

	/*
	 * Convert based on scale ratio.
	 */
	switch (axis) {
	case AXIS_X:
		ratio = priv->width / (priv->end_x - priv->begin_x);
		*out_coord = ratio * in_coord;
		break;
	case AXIS_Y:
		ratio = priv->height / (priv->end_y - priv->begin_y);
		*out_coord = ratio * in_coord;
		break;
	default:
		g_warn_if_reached();
		return FALSE;
	}

	return TRUE;
}

static gdouble
get_column_spacing (PkgScaleLinear *scale)
{
	PkgScaleLinearPrivate *priv;
	gdouble data_width, scale_width;

	g_return_val_if_fail(PKG_IS_SCALE_LINEAR(scale), 0.);

	priv = scale->priv;

	data_width = ABS(priv->end_x - priv->begin_x);
	scale_width = clutter_actor_get_width(CLUTTER_ACTOR(scale));

	if (data_width == 0) {
		return scale_width / 10.;
	}

	g_warn_if_reached(); // TODO

	return 25.;
}

static gdouble
get_row_spacing (PkgScaleLinear *scale)
{
	PkgScaleLinearPrivate *priv;
	gdouble data_height, scale_height;

	g_return_val_if_fail(PKG_IS_SCALE_LINEAR(scale), 0.);

	priv = scale->priv;

	data_height = ABS(priv->end_y - priv->begin_y);
	scale_height = clutter_actor_get_height(CLUTTER_ACTOR(scale));

	if (data_height == 0) {
		return scale_height / 10.;
	}

	g_warn_if_reached(); // TODO

	return 25.;

}

static void
pkg_scale_linear_paint (ClutterActor *actor)
{

	PkgScaleLinearPrivate *priv;
	const gdouble dashes[] = {1., 2.};
	PangoFontDescription *font_desc;
	PangoLayout *layout;
	cairo_t *cr;
	gfloat w, h;
	gdouble x, x0, x1, x2;
	gdouble y, y0, y1, y2;
	gint lw, lh;
	gdouble spacing;

	g_return_if_fail(PKG_IS_SCALE_LINEAR(actor));

	/*
	 * Get parameters for drawing.
	 */
	priv = PKG_SCALE_LINEAR(actor)->priv;
	clutter_actor_get_size(actor, &w, &h);

	/*
	 * Clear the texture.
	 */
	clutter_cairo_texture_clear(CLUTTER_CAIRO_TEXTURE(actor));

	/*
	 * Determine various offsets for drawing.
	 */
	x0 = priv->label_spacing;
	x1 = floor(priv->label_spacing) + LINE_STUB_SIZE;
	x2 = floor(w) - .5;
	y0 = floor(h) - priv->label_spacing;
	y1 = y0 - LINE_STUB_SIZE;
	y2 = .5;

	/*
	 * Create cairo context and set default drawing parameters.
	 */
	cr = clutter_cairo_texture_create(CLUTTER_CAIRO_TEXTURE(actor));
	cairo_set_line_width(cr, 1.);
	cairo_set_dash(cr, dashes, 2, 0.);

	/*
	 * Fill the background of the scale in with the background color.
	 */
	cairo_rectangle(cr, x1, y2, x2, y1);
	cairo_set_source_rgb(cr, 1., 1., 1.);
	cairo_fill(cr);

	/*
	 * Draw the outer lines of the scale.
	 */
	cairo_set_source_rgb(cr, 0., 0., 0.);
	cairo_move_to(cr, x1, y0);
	cairo_line_to(cr, x1, y2);
	cairo_move_to(cr, x0, y2);
	cairo_line_to(cr, x2, y2);
	cairo_move_to(cr, x0, y1);
	cairo_line_to(cr, x2, y1);
	cairo_move_to(cr, x2, y2);
	cairo_line_to(cr, x2, y0);
	cairo_stroke(cr);

	/*
	 * Draw the X axis grid.
	 */
	spacing = get_column_spacing(PKG_SCALE_LINEAR(actor));
	for (x = x1 + spacing; x < (floor(w) - .5); x += spacing) {
		cairo_move_to(cr, x, y0);
		cairo_line_to(cr, x, y2);
	}
	cairo_stroke(cr);

	/*
	 * Draw the Y axis grid.
	 */
	spacing = get_row_spacing(PKG_SCALE_LINEAR(actor));
	for (y = y1 - spacing; y > y2; y -= spacing) {
		cairo_move_to(cr, x0, y);
		cairo_line_to(cr, x2, y);
	}
	cairo_stroke(cr);

	/*
	 * Draw the X axis label.
	 */
	layout = pango_cairo_create_layout(cr);
	font_desc = pango_font_description_from_string("Bold Sans 10");
	pango_layout_set_font_description(layout, font_desc);
	pango_layout_set_markup(layout, "<span size=\"smaller\">X Axis</span>", -1);
	pango_layout_get_pixel_size(layout, &lw, &lh);
	cairo_move_to(cr, x1 + ((x2 - x1 - lw) / 2.), h - lh);
	pango_cairo_show_layout(cr, layout);
	g_object_unref(layout);
	pango_font_description_free(font_desc);

	/*
	 * Draw the Y axis label.
	 */
	layout = pango_cairo_create_layout(cr);
	font_desc = pango_font_description_from_string("Bold Sans 10");
	pango_layout_set_font_description(layout, font_desc);
	pango_layout_set_markup(layout, "<span size=\"smaller\">Y Axis</span>", -1);
	pango_layout_get_pixel_size(layout, &lw, &lh);
	cairo_move_to(cr, .5, (y1 - ((y1 - lw) / 2.)));
	cairo_rotate(cr, M_PI / -2.);
	pango_cairo_show_layout(cr, layout);
	g_object_unref(layout);
	pango_font_description_free(font_desc);

	/*
	 * Clean up our cairo context and release resources.
	 */
	cairo_destroy(cr);


	/*
	cairo_rectangle(cr, 10.5, 0.5, w - 11., h - 11.);
	cairo_set_source_rgb(cr, 1., 1., 1.);
	cairo_fill(cr);
	space = h / 5.;
	cairo_move_to(cr, 0., .5);
	cairo_line_to(cr, w, .5);
	for (i = space; i < h; i += space) {
		cairo_move_to(cr, 0., round(i) + .5);
		cairo_line_to(cr, w, round(i) + .5);
	}
	space = ABS(w - 11.) / 5.;
	for (i = 9.5; i < w; i += space) {
		cairo_move_to(cr, round(i) + .5, 0.);
		cairo_line_to(cr, round(i) + .5, h);
	}
	cairo_move_to(cr, 0., h - 10.5);
	cairo_line_to(cr, w, h - 10.5);
	cairo_move_to(cr, w - .5, 0.);
	cairo_line_to(cr, w - .5, h);
	cairo_set_line_width(cr, 1.);
	cairo_set_source_rgba(cr, 0, 0, 0, 1.);
	cairo_set_dash(cr, dashes, 2, 0.);
	cairo_stroke(cr);
	cairo_destroy(cr);
	*/
}

static void
pkg_scale_linear_allocation_changed (ClutterActor           *actor,
                                     const ClutterActorBox  *box,
                                     ClutterAllocationFlags  flags)
{
	clutter_cairo_texture_set_surface_size(CLUTTER_CAIRO_TEXTURE(actor),
	                                       clutter_actor_box_get_width(box),
	                                       clutter_actor_box_get_height(box));
	pkg_scale_linear_paint(actor);
}

static void
pkg_scale_linear_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_scale_linear_parent_class)->finalize(object);
}

static void
pkg_scale_linear_class_init (PkgScaleLinearClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_scale_linear_finalize;
	g_type_class_add_private(object_class, sizeof(PkgScaleLinearPrivate));
}

static void
pkg_scale_linear_init (PkgScaleLinear *scale)
{
	scale->priv = G_TYPE_INSTANCE_GET_PRIVATE(scale,
	                                          PKG_TYPE_SCALE_LINEAR,
	                                          PkgScaleLinearPrivate);
	scale->priv->label_spacing = 15.;

	clutter_actor_set_size(CLUTTER_ACTOR(scale), 10, 10);

	g_signal_connect(scale,
	                 "allocation-changed",
	                 G_CALLBACK(pkg_scale_linear_allocation_changed),
	                 NULL);
}

static void
pkg_scale_init (PkgScaleIface *iface)
{
	iface->translate = pkg_scale_linear_real_translate;
}
