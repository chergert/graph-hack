/* pkg-graph.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-graph.h"

/**
 * SECTION:pkg-graph
 * @title: PkgGraph
 * @short_description: 
 *
 * Section overview.
 */

G_DEFINE_TYPE(PkgGraph, pkg_graph, CLUTTER_TYPE_GROUP)

struct _PkgGraphPrivate
{
	ClutterActor *renderer;
	ClutterActor *scale;
	GHashTable   *attrs;
	GList        *sets;
	gint          xpad;
	gint          ypad;
};

typedef struct
{
	gchar *attribute;
	gint   axis;
} PkgGraphAttr;

/**
 * pkg_graph_new:
 *
 * Creates a new instance of #PkgGraph.
 *
 * Returns: the newly created instance of #PkgGraph.
 * Side effects: None.
 */
ClutterActor*
pkg_graph_new (void)
{
	return g_object_new(PKG_TYPE_GRAPH, NULL);
}

/**
 * pkg_graph_clear_attributes:
 * @graph: A #PkgGraph.
 * @data_set: A #PkgDataSet.
 *
 * Clears all attributes mapped with a #GDataSet.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_clear_attributes (PkgGraph   *graph,
                            PkgDataSet *data_set)
{
	PkgGraphPrivate *priv;
	GArray *array;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_DATA_SET(data_set));

	priv = graph->priv;

	if (!(array = g_hash_table_lookup(priv->attrs, data_set))) {
		g_warning("PkgDataSet %p not added to PkgGraph %p",
		          data_set, graph);
		return;
	}

	g_array_remove_range(array, 0, array->len);
}

/**
 * pkg_graph_add_attribute:
 * @graph: A #PkgGraph.
 *
 * Adds an attribute mapping to the graph.  The attribute mapping is used
 * to set attributes on the attached #PkgRenderer instance.  The value of
 * the mapping is retrieved from the data set.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_add_attribute (PkgGraph    *graph,
                         PkgDataSet  *data_set,
                         const gchar *attribute,
                         gint         axis)
{
	PkgGraphPrivate *priv;
	PkgGraphAttr graph_attr;
	GArray *attrs;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_DATA_SET(data_set));
	g_return_if_fail(attribute != NULL);

	priv = graph->priv;

	if (!(attrs = g_hash_table_lookup(priv->attrs, data_set))) {
		g_warning("Invalid data set for graph instance %p", graph);
		return;
	}

	graph_attr.attribute = g_strdup(attribute);
	graph_attr.axis = axis;
	g_array_append_val(attrs, graph_attr);
}

/**
 * pkg_graph_set_attributes:
 * @graph: A #PkgGraph.
 *
 * Maps a set of attributes for the data set to be rendered on the graph.
 * The previous attributes are cleared before setting the new attributes.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_set_attributes (PkgGraph    *graph,
                          PkgDataSet  *data_set,
                          const gchar *first_attribute,
                          ...)
{
	const gchar *attribute;
	va_list args;
	gint axis;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_DATA_SET(data_set));

	pkg_graph_clear_attributes(graph, data_set);
	attribute = first_attribute;

	va_start(args, first_attribute);
	do {
		axis = va_arg(args, gint);
		pkg_graph_add_attribute(graph, data_set, attribute, axis);
		attribute = va_arg(args, const gchar *);
	} while (attribute != NULL);
	va_end(args);
}

/**
 * pkg_graph_add_data_set:
 * @graph: A #PkgGraph.
 * @data_set: A #PkgDataSet.
 *
 * Adds a data set to the list of data sets that are rendered on the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_add_data_set (PkgGraph   *graph,
                        PkgDataSet *data_set)
{
	PkgGraphPrivate *priv;
	GArray *array;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_DATA_SET(data_set));

	priv = graph->priv;

	if (g_hash_table_lookup(priv->attrs, data_set)) {
		g_warning("PkgDataSet %p already added to PkgGraphInstance %p",
		          data_set, graph);
		return;
	}

	array = g_array_new(FALSE, FALSE, sizeof(PkgGraphAttr));
	g_hash_table_insert(priv->attrs, data_set, array);
	priv->sets = g_list_prepend(priv->sets, data_set);
}

static void
pkg_graph_place_child (PkgGraph     *graph,
                       ClutterActor *child)
{
	PkgGraphPrivate *priv;
	gfloat w, h, x = 0, y = 0;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(CLUTTER_IS_ACTOR(child));

	priv = graph->priv;

	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);

	if (priv->xpad) {
		x = priv->xpad;
	}

	if (priv->ypad) {
		y = priv->ypad;
	}

	w -= 2 * priv->xpad;
	h -= 2 * priv->ypad;

	clutter_actor_set_size(child, w, h);
	clutter_actor_set_position(child, x, y);
	clutter_actor_queue_redraw(child);
}

/**
 * pkg_graph_set_scale:
 * @graph: A #PkgGraph.
 *
 * Sets the #PkgScale for the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_set_scale (PkgGraph *graph,
                     PkgScale *scale)
{
	PkgGraphPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_SCALE(scale));

	priv = graph->priv;

	if (priv->scale) {
		clutter_container_remove_actor(CLUTTER_CONTAINER(graph), priv->scale);
		g_object_remove_weak_pointer(G_OBJECT(priv->scale),
		                             (gpointer *)&priv->scale);
	}

	priv->scale = CLUTTER_ACTOR(scale);
	g_object_add_weak_pointer(G_OBJECT(scale), (gpointer *)&priv->scale);

	clutter_container_add_actor(CLUTTER_CONTAINER(graph),
	                            CLUTTER_ACTOR(scale));
	pkg_graph_place_child(graph, CLUTTER_ACTOR(scale));
}

/**
 * pkg_graph_set_renderer:
 * @graph: A #PkgGraph.
 *
 * Sets the #PkgRenderer for the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_set_renderer (PkgGraph    *graph,
                        PkgRenderer *renderer)
{
	PkgGraphPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_RENDERER(renderer));

	priv = graph->priv;

	if (priv->renderer) {
		clutter_container_remove_actor(CLUTTER_CONTAINER(graph),
		                               CLUTTER_ACTOR(priv->renderer));
		g_object_remove_weak_pointer(G_OBJECT(priv->renderer),
		                             (gpointer *)&priv->renderer);
	}

	priv->renderer = CLUTTER_ACTOR(renderer);
	g_object_add_weak_pointer(G_OBJECT(renderer),
	                          (gpointer *)&priv->renderer);

	clutter_container_add_actor(CLUTTER_CONTAINER(graph),
	                            CLUTTER_ACTOR(renderer));
	pkg_graph_place_child(graph, CLUTTER_ACTOR(renderer));
}

/**
 * pkg_graph_set_padding:
 * @graph: A #PkgGraph.
 * @xpad: The amount of xpadding or -1 to ignore.
 * @ypad: The amount of ypadding or -1 to ignore.
 *
 * Sets the outer padding around the graph.  You may set @xpad or @ypad to
 * -1 to ignore that argument.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_set_padding (PkgGraph *graph,
                       gint      xpad,
                       gint      ypad)
{
	PkgGraphPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH(graph));

	priv = graph->priv;

	if (xpad > 0) {
		priv->xpad = xpad;
		if (priv->scale) {
			clutter_actor_set_x(CLUTTER_ACTOR(priv->scale), xpad);
		}
		if (priv->renderer) {
			clutter_actor_set_x(CLUTTER_ACTOR(priv->renderer), xpad);
		}
	}

	if (ypad > 0) {
		priv->ypad = ypad;
		if (priv->scale) {
			clutter_actor_set_y(CLUTTER_ACTOR(priv->scale), ypad);
		}
		if (priv->renderer) {
			clutter_actor_set_y(CLUTTER_ACTOR(priv->renderer), ypad);
		}
	}
}

static void
pkg_graph_real_allocate (ClutterActor           *actor,
                         const ClutterActorBox  *box,
                         ClutterAllocationFlags  flags)
{
	PkgGraphPrivate *priv;

	CLUTTER_ACTOR_CLASS(pkg_graph_parent_class)->allocate(actor, box, flags);

	priv = PKG_GRAPH(actor)->priv;

	if (priv->scale) {
		pkg_graph_place_child(PKG_GRAPH(actor), priv->scale);
	}

	if (priv->renderer) {
		pkg_graph_place_child(PKG_GRAPH(actor), priv->renderer);
	}
}

static void
pkg_graph_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_graph_parent_class)->finalize(object);
}

static void
pkg_graph_class_init (PkgGraphClass *klass)
{
	GObjectClass *object_class;
	ClutterActorClass *actor_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_graph_finalize;
	g_type_class_add_private(object_class, sizeof(PkgGraphPrivate));

	actor_class = CLUTTER_ACTOR_CLASS(klass);
	actor_class->allocate = pkg_graph_real_allocate;
}

static void
pkg_graph_init (PkgGraph *graph)
{
	graph->priv = G_TYPE_INSTANCE_GET_PRIVATE(graph,
	                                          PKG_TYPE_GRAPH,
	                                          PkgGraphPrivate);

	/* hash table for data set attribute mappings */
	graph->priv->attrs = g_hash_table_new_full(g_direct_hash,
	                                           g_direct_equal,
	                                           NULL,
	                                           (GDestroyNotify)g_array_unref);
}
