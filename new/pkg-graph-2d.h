/* pkg-graph-2d.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_GRAPH_2D_H__
#define __PKG_GRAPH_2D_H__

#include "pkg-graph.h"
#include "pkg-scale.h"

G_BEGIN_DECLS

#define PKG_TYPE_GRAPH_2D            (pkg_graph_2d_get_type())
#define PKG_GRAPH_2D(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH_2D, PkgGraph2d))
#define PKG_GRAPH_2D_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH_2D, PkgGraph2d const))
#define PKG_GRAPH_2D_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_GRAPH_2D, PkgGraph2dClass))
#define PKG_IS_GRAPH_2D(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_GRAPH_2D))
#define PKG_IS_GRAPH_2D_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_GRAPH_2D))
#define PKG_GRAPH_2D_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_GRAPH_2D, PkgGraph2dClass))

typedef struct _PkgGraph2d        PkgGraph2d;
typedef struct _PkgGraph2dClass   PkgGraph2dClass;
typedef struct _PkgGraph2dPrivate PkgGraph2dPrivate;

struct _PkgGraph2d
{
	ClutterGroup parent;

	/*< private >*/
	PkgGraph2dPrivate *priv;
};

struct _PkgGraph2dClass
{
	ClutterGroupClass parent_class;
};

GType         pkg_graph_2d_get_type    (void) G_GNUC_CONST;
ClutterActor* pkg_graph_2d_new         (void);
void          pkg_graph_2d_set_x_scale (PkgGraph2d *graph,
                                        PkgScale   *scale);
void          pkg_graph_2d_set_y_scale (PkgGraph2d *graph,
                                        PkgScale   *scale);
PkgScale*     pkg_graph_2d_get_x_scale (PkgGraph2d *graph);
PkgScale*     pkg_graph_2d_get_y_scale (PkgGraph2d *graph);

G_END_DECLS

#endif /* __PKG_GRAPH_2D_H__ */
