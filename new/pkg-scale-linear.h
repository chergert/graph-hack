/* pkg-scale-linear.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PKG_SCALE_LINEAR_H__
#define __PKG_SCALE_LINEAR_H__

#include "pkg-scale.h"

G_BEGIN_DECLS

#define PKG_TYPE_SCALE_LINEAR            (pkg_scale_linear_get_type())
#define PKG_SCALE_LINEAR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_SCALE_LINEAR, PkgScaleLinear))
#define PKG_SCALE_LINEAR_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_SCALE_LINEAR, PkgScaleLinear const))
#define PKG_SCALE_LINEAR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_SCALE_LINEAR, PkgScaleLinearClass))
#define PKG_IS_SCALE_LINEAR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_SCALE_LINEAR))
#define PKG_IS_SCALE_LINEAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_SCALE_LINEAR))
#define PKG_SCALE_LINEAR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_SCALE_LINEAR, PkgScaleLinearClass))

typedef struct _PkgScaleLinear        PkgScaleLinear;
typedef struct _PkgScaleLinearClass   PkgScaleLinearClass;
typedef struct _PkgScaleLinearPrivate PkgScaleLinearPrivate;

struct _PkgScaleLinear
{
	GInitiallyUnowned parent;

	/*< private >*/
	PkgScaleLinearPrivate *priv;
};

struct _PkgScaleLinearClass
{
	GInitiallyUnownedClass parent_class;
};

GType     pkg_scale_linear_get_type (void) G_GNUC_CONST;
PkgScale* pkg_scale_linear_new      (void);

G_END_DECLS

#endif /* __PKG_SCALE_LINEAR_H__ */
