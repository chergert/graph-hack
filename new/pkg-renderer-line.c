/* pkg-renderer-line.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "pkg-renderer-line.h"

/**
 * SECTION:pkg-renderer-line
 * @title: PkgRendererLine
 * @short_description: 
 *
 * Section overview.
 */

static void pkg_renderer_init (PkgRendererIface *iface);

G_DEFINE_TYPE_EXTENDED(PkgRendererLine,
                       pkg_renderer_line,
                       CLUTTER_TYPE_GROUP,
                       0,
                       G_IMPLEMENT_INTERFACE(PKG_TYPE_RENDERER,
                                             pkg_renderer_init))

enum
{
	PROP_0,

	PROP_X_DATA,
	PROP_Y_DATA,
};

struct _PkgRendererLinePrivate
{
	ClutterActor *texture;
	gdouble       x_data;
	gdouble       y_data;
	PkgScale     *x_scale;
	PkgScale     *y_scale;

	gdouble       last_x;
	gdouble       last_y;
};

void
pkg_renderer_line_set_x_scale (PkgRendererLine *renderer,
                               PkgScale        *scale)
{
	renderer->priv->last_x = renderer->priv->x_data;
	renderer->priv->x_scale = g_object_ref(scale);
}

void
pkg_renderer_line_set_y_scale (PkgRendererLine *renderer,
                               PkgScale        *scale)
{
	renderer->priv->last_y = renderer->priv->y_data;
	renderer->priv->y_scale = g_object_ref(scale);
}

/**
 * pkg_renderer_line_new:
 *
 * Creates a new instance of #PkgRendererLine.
 *
 * Returns: the newly created instance of #PkgRendererLine.
 * Side effects: None.
 */
ClutterActor*
pkg_renderer_line_new (void)
{
	return g_object_new(PKG_TYPE_RENDERER_LINE, NULL);
}

/**
 * pkg_renderer_line_set_x_data:
 * @renderer: A #PkgRendererLine.
 *
 * Sets the x-data property of @renderer.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_renderer_line_set_x_data (PkgRendererLine *renderer,
                              gdouble          x_data)
{
	g_return_if_fail(renderer != NULL);
	renderer->priv->x_data = x_data;
}

/**
 * pkg_renderer_line_set_y_data:
 * @renderer: A #PkgRendererLine.
 *
 * Sets the y-data property of @renderer.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_renderer_line_set_y_data (PkgRendererLine *renderer,
                              gdouble          y_data)
{
	g_return_if_fail(renderer != NULL);
	renderer->priv->y_data = y_data;
}

static void
pkg_renderer_line_real_render (PkgRenderer *renderer)
{
	PkgRendererLinePrivate *priv;
	ClutterCairoTexture *ct;
	PkgScale *x_scale, *y_scale; /* TODO: HOW DO WE GET THESE? */
	cairo_t *cr;
	gdouble x, y;

	priv = PKG_RENDERER_LINE(renderer)->priv;
	ct = CLUTTER_CAIRO_TEXTURE(priv->texture);
	cr = clutter_cairo_texture_create(ct);

	/*
	 * Translate the values.
	 */
	x_scale = priv->x_scale;
	y_scale = priv->y_scale;
	if (!pkg_scale_translate(x_scale, priv->x_data, &x) ||
	    !pkg_scale_translate(y_scale, priv->y_data, &y)) {
	    goto cleanup;
	}

	g_debug("Drawing circle at %f,%f", x, y);

	if (priv->last_x != -1 && priv->last_y != -1) {
		cairo_move_to(cr, priv->last_x, priv->last_y);
		cairo_line_to(cr, x, y);
		cairo_stroke(cr);
	}

	priv->last_x = x;
	priv->last_y = y;

	//cairo_rectangle(cr, 0, 0, 1000, 1000);
	cairo_arc(cr, x, y, 2., 0., 2 * M_PI);
	cairo_set_source_rgb(cr, 0., 0., 0.);
	cairo_fill(cr);

cleanup:
	cairo_destroy(cr);
	clutter_actor_show(CLUTTER_ACTOR(renderer));
	clutter_actor_show(priv->texture);
	clutter_actor_set_position(CLUTTER_ACTOR(renderer), 0, 0);
	clutter_actor_set_position(CLUTTER_ACTOR(priv->texture), 0, 0);
	clutter_actor_set_size(CLUTTER_ACTOR(renderer), 1000, 1000);
	clutter_actor_set_size(CLUTTER_ACTOR(priv->texture), 1000, 1000);
	clutter_actor_queue_redraw(priv->texture);
	clutter_actor_queue_redraw(CLUTTER_ACTOR(renderer));
}

static void
pkg_renderer_line_allocation_changed (ClutterActor           *actor,
                                      const ClutterActorBox  *box,
                                      ClutterAllocationFlags  flags)
{
	PkgRendererLinePrivate *priv;

	g_debug("%s", G_STRLOC);

	g_return_if_fail(PKG_IS_RENDERER_LINE(actor));
	g_return_if_fail(box != NULL);

	priv = PKG_RENDERER_LINE(actor)->priv;

	g_debug("%f,%f", 
			clutter_actor_box_get_width(box),
			clutter_actor_box_get_height(box));

	clutter_cairo_texture_clear(CLUTTER_CAIRO_TEXTURE(priv->texture));

	clutter_actor_set_size(
			priv->texture,
			clutter_actor_box_get_width(box),
			clutter_actor_box_get_height(box));
	clutter_cairo_texture_set_surface_size(
			CLUTTER_CAIRO_TEXTURE(priv->texture),
			clutter_actor_box_get_width(box),
			clutter_actor_box_get_height(box));
}

static void
pkg_renderer_line_real_clear (PkgRenderer *renderer)
{
	PkgRendererLinePrivate *priv;

	priv = PKG_RENDERER_LINE(renderer)->priv;

	clutter_cairo_texture_clear(CLUTTER_CAIRO_TEXTURE(priv->texture));
	priv->last_x = -1;
	priv->last_y = -1;
}

static void
pkg_renderer_line_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_renderer_line_parent_class)->finalize(object);
}

static void
pkg_renderer_line_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
	switch (prop_id) {
	case PROP_X_DATA:
		pkg_renderer_line_set_x_data(PKG_RENDERER_LINE(object),
		                             g_value_get_double(value));
		break;
	case PROP_Y_DATA:
		pkg_renderer_line_set_y_data(PKG_RENDERER_LINE(object),
		                             g_value_get_double(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
pkg_renderer_line_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
	switch (prop_id) {
	case PROP_X_DATA:
		g_value_set_double(value, PKG_RENDERER_LINE(object)->priv->x_data);
		break;
	case PROP_Y_DATA:
		g_value_set_double(value, PKG_RENDERER_LINE(object)->priv->y_data);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
pkg_renderer_line_class_init (PkgRendererLineClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_renderer_line_finalize;
	object_class->set_property = pkg_renderer_line_set_property;
	object_class->get_property = pkg_renderer_line_get_property;
	g_type_class_add_private(object_class, sizeof(PkgRendererLinePrivate));

	/**
	 * PkgRendererLine:x-data:
	 *
	 * The "x-data" property.
	 */
	g_object_class_install_property(object_class,
	                                PROP_X_DATA,
	                                g_param_spec_double("x-data",
	                                                    "x-data",
	                                                    "x-data",
	                                                    -G_MAXDOUBLE,
	                                                    G_MAXDOUBLE,
	                                                    0.,
	                                                    G_PARAM_READWRITE));

	/**
	 * PkgRendererLine:y-data:
	 *
	 * The "y-data" property.
	 */
	g_object_class_install_property(object_class,
	                                PROP_Y_DATA,
	                                g_param_spec_double("y-data",
	                                                    "y-data",
	                                                    "y-data",
	                                                    -G_MAXDOUBLE,
	                                                    G_MAXDOUBLE,
	                                                    0.,
	                                                    G_PARAM_READWRITE));

}

static void
pkg_renderer_line_init (PkgRendererLine *renderer)
{
	renderer->priv = G_TYPE_INSTANCE_GET_PRIVATE(renderer,
	                                       PKG_TYPE_RENDERER_LINE,
	                                       PkgRendererLinePrivate);

	renderer->priv->texture = clutter_cairo_texture_new(1000, 1000);
	clutter_actor_set_size(renderer->priv->texture, 1000, 1000);
	clutter_container_add_actor(CLUTTER_CONTAINER(renderer),
	                            renderer->priv->texture);
	clutter_actor_show(renderer->priv->texture);

	g_signal_connect(renderer,
	                 "allocation-changed",
	                 G_CALLBACK(pkg_renderer_line_allocation_changed),
	                 NULL);
}

static void
pkg_renderer_init (PkgRendererIface *iface)
{
	iface->render = pkg_renderer_line_real_render;
	iface->clear = pkg_renderer_line_real_clear;
}
