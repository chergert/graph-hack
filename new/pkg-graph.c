/* pkg-graph.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-graph.h"

#define PKG_GRAPH_PRIVATE (pkg_graph_private_quark())

typedef struct
{
	GList *matrixes;
	GList *attributes;
} PkgGraphPrivate;

static inline GQuark
pkg_graph_private_quark (void)
{
	return g_quark_from_static_string("pkg-graph-matrixes");
}

static void
pkg_graph_attribute_free (PkgGraphAttribute *attribute)
{
	g_object_unref(attribute->matrix);
	g_object_unref(attribute->renderer);
	g_free(attribute->attribute);
	g_slice_free(PkgGraphAttribute, attribute);
}

static void
pkg_graph_private_free (gpointer user_data)
{
	PkgGraphPrivate *priv = user_data;
	g_assert(priv);

	g_list_foreach(priv->matrixes, (GFunc)g_object_unref, NULL);
	g_list_foreach(priv->attributes, (GFunc)pkg_graph_attribute_free, NULL);
	g_list_free(priv->matrixes);
	g_list_free(priv->attributes);
	g_slice_free(PkgGraphPrivate, priv);
}

/**
 * pkg_graph_get_matrixes:
 * @graph: A #PkgGraph.
 *
 * Retrieves the list of #PkgMatrix<!-- -->'s associated with the graph.
 *
 * Returns: A #GList containing the matrixes.  The list should not be
 *   modified or freed.
 * Side effects: None.
 */
GList*
pkg_graph_get_matrixes (PkgGraph *graph)
{
	PkgGraphPrivate *priv;

	g_return_val_if_fail(PKG_IS_GRAPH(graph), NULL);

	if (!(priv = g_object_get_qdata(G_OBJECT(graph), PKG_GRAPH_PRIVATE))) {
		return NULL;
	}

	return priv->matrixes;
}

/**
 * pkg_graph_get_attributes:
 * @graph: A #PkgGraph.
 *
 * Retrieves the attributes attached to a given #PkgGraph instance.
 *
 * Returns: A #GList of #PkgGraphAttribute.  The list should not be modified
 *   or freed.
 * Side effects: None.
 */
GList*
pkg_graph_get_attributes (PkgGraph *graph)
{
	PkgGraphPrivate *priv;

	g_return_val_if_fail(PKG_IS_GRAPH(graph), NULL);

	if (!(priv = g_object_get_qdata(G_OBJECT(graph), PKG_GRAPH_PRIVATE))) {
		return NULL;
	}

	return priv->attributes;
}

/**
 * pkg_graph_pack_renderer:
 * @graph: A #PkgGraph.
 * @renderer: A #PkgRenderer.
 *
 * Adds a #PkgRenderer to the set of renderers on the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_pack_renderer (PkgGraph    *graph,
                         PkgRenderer *renderer)
{
	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_RENDERER(renderer));

	PKG_GRAPH_GET_INTERFACE(graph)->pack_renderer(graph, renderer);
}

/**
 * pkg_graph_add_matrix:
 * @graph: A #PkgGraph.
 *
 * Adds matrix to the set of available matrixes for drawing within the
 * graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_add_matrix (PkgGraph  *graph,
                      PkgMatrix *matrix)
{
	PkgGraphPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_MATRIX(matrix));

	priv = g_object_get_qdata(G_OBJECT(graph), PKG_GRAPH_PRIVATE);

	if (!priv) {
		priv = g_slice_new0(PkgGraphPrivate);
		g_object_set_qdata_full(G_OBJECT(graph), PKG_GRAPH_PRIVATE, priv,
		                        pkg_graph_private_free);
	}

	priv->matrixes = g_list_prepend(priv->matrixes, g_object_ref(matrix));
}

/**
 * pkg_graph_add_attribute:
 * @graph: A #PggGraph.
 * @renderer: A #PkgRenderer.
 * @matrix: A #PkgMatrix.
 * @attribute: The attribute name.
 * @column: The column within the matrix containing the value.
 *
 * Adds an attribute to the list of attributes to be applied when
 * renderering.  Attributes are used to map columns within rows to
 * particular properties of a #PkgRenderer.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_add_attribute (PkgGraph    *graph,
                         PkgRenderer *renderer,
                         PkgMatrix   *matrix,
                         const gchar *attribute,
                         gint         column)
{
	PkgGraphPrivate *priv;
	PkgGraphAttribute *attr;

	g_return_if_fail(PKG_IS_GRAPH(graph));
	g_return_if_fail(PKG_IS_RENDERER(renderer));
	g_return_if_fail(PKG_IS_MATRIX(matrix));
	g_return_if_fail(attribute != NULL);

	priv = g_object_get_qdata(G_OBJECT(graph), PKG_GRAPH_PRIVATE);

	if (!priv) {
		g_warning("PkgMatrix %p not packed in PkgGraph %p.", matrix, graph);
		return;
	}

	attr = g_slice_new0(PkgGraphAttribute);
	attr->attribute = g_strdup(attribute);
	attr->matrix = g_object_ref(matrix);
	attr->renderer = g_object_ref(renderer);
	attr->column = column;

	priv->attributes = g_list_prepend(priv->attributes, attr);
}

/**
 * pkg_graph_render:
 * @graph: A #PkgGraph.
 *
 * Renders the graph using the scale, renderers, and matrixes applied.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_render (PkgGraph *graph)
{
	g_return_if_fail(PKG_IS_GRAPH(graph));

	PKG_GRAPH_GET_INTERFACE(graph)->render(graph);
}

GType
pkg_graph_get_type (void)
{
	static GType type_id = 0;

	if (g_once_init_enter((gsize *)&type_id)) {
		GType _type_id;
		const GTypeInfo g_type_info = {
			sizeof(PkgGraphIface),
			NULL, /* base_init */
			NULL, /* base_finalize */
			NULL, /* class_init */
			NULL, /* class_finalize */
			NULL, /* class_data */
			0,    /* instance_size */
			0,    /* n_preallocs */
			NULL, /* instance_init */
			NULL  /* value_vtable */
		};

		_type_id = g_type_register_static(G_TYPE_INTERFACE,
		                                  "PkgGraph",
		                                  &g_type_info,
		                                  0);
		g_type_interface_add_prerequisite(_type_id, CLUTTER_TYPE_ACTOR);
		g_once_init_leave((gsize *)&type_id, _type_id);
	}

	return type_id;
}
