/* pkg-renderer-line.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_RENDERER_LINE_H__
#define __PKG_RENDERER_LINE_H__

#include <clutter/clutter.h>

#include "pkg-renderer.h"
#include "pkg-scale.h"

G_BEGIN_DECLS

#define PKG_TYPE_RENDERER_LINE            (pkg_renderer_line_get_type())
#define PKG_RENDERER_LINE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_RENDERER_LINE, PkgRendererLine))
#define PKG_RENDERER_LINE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_RENDERER_LINE, PkgRendererLine const))
#define PKG_RENDERER_LINE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_RENDERER_LINE, PkgRendererLineClass))
#define PKG_IS_RENDERER_LINE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_RENDERER_LINE))
#define PKG_IS_RENDERER_LINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_RENDERER_LINE))
#define PKG_RENDERER_LINE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_RENDERER_LINE, PkgRendererLineClass))

typedef struct _PkgRendererLine        PkgRendererLine;
typedef struct _PkgRendererLineClass   PkgRendererLineClass;
typedef struct _PkgRendererLinePrivate PkgRendererLinePrivate;

struct _PkgRendererLine
{
	ClutterGroup parent;

	/*< private >*/
	PkgRendererLinePrivate *priv;
};

struct _PkgRendererLineClass
{
	ClutterGroupClass parent_class;
};

GType         pkg_renderer_line_get_type    (void) G_GNUC_CONST;
ClutterActor* pkg_renderer_line_new         (void);
void          pkg_renderer_line_set_x_data  (PkgRendererLine *renderer,
                                             gdouble          x_data);
void          pkg_renderer_line_set_y_data  (PkgRendererLine *renderer,
                                             gdouble          y_data);
void          pkg_renderer_line_set_x_scale (PkgRendererLine *renderer,
                                             PkgScale        *x_scale);
void          pkg_renderer_line_set_y_scale (PkgRendererLine *renderer,
                                             PkgScale        *y_scale);



G_END_DECLS

#endif /* __PKG_RENDERER_LINE_H__ */
