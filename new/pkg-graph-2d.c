/* pkg-graph-2d.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pango/pangocairo.h>

#include "pkg-graph-2d.h"
#include "pkg-scale-linear.h"

#define STUB_LENGTH 15

/**
 * SECTION:pkg-graph-2d
 * @title: PkgGraph2d
 * @short_description: 
 *
 * Section overview.
 */

static void pkg_graph_init (PkgGraphIface *iface);

G_DEFINE_TYPE_EXTENDED(PkgGraph2d,
                       pkg_graph_2d,
                       CLUTTER_TYPE_GROUP,
                       0,
                       G_IMPLEMENT_INTERFACE(PKG_TYPE_GRAPH,
                                             pkg_graph_init))

struct _PkgGraph2dPrivate
{
	ClutterActor *texture;
	ClutterActor *renderer;
	PkgScale     *x_scale;
	PkgScale     *y_scale;
	gdouble       x_padding;
	gdouble       y_padding;
	gchar        *x_label;
	gchar        *y_label;
};

/**
 * pkg_graph_2d_new:
 *
 * Creates a new instance of #PkgGraph2d.
 *
 * Returns: the newly created instance of #PkgGraph2d.
 * Side effects: None.
 */
ClutterActor*
pkg_graph_2d_new (void)
{
	return g_object_new(PKG_TYPE_GRAPH_2D, NULL);
}

static void
pkg_graph_2d_get_primary_extents (PkgGraph2d *graph,
                                  gdouble    *x0,
                                  gdouble    *x1,
                                  gdouble    *x2,
                                  gdouble    *y0,
                                  gdouble    *y1,
                                  gdouble    *y2)
{
	PkgGraph2dPrivate *priv = graph->priv;
	gfloat w, h;

	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);

	*x0 = priv->x_padding;
	*x1 = *x0 + STUB_LENGTH + .5;
	*x2 = w - .5;
	*y0 = h - priv->y_padding - .5;
	*y1 = *y0 - STUB_LENGTH;
	*y2 = .5;
}

static void
pkg_graph_2d_paint_children (PkgGraph2d *graph)
{
	GList *aall, *alist;
	GList *mall, *mlist;

	mall = pkg_graph_get_matrixes(PKG_GRAPH(graph));
	aall = pkg_graph_get_attributes(PKG_GRAPH(graph));

	/*
	 * XXX:
	 *
	 *    We need a good way to map the many-to-many relationship here.  The
	 *    matrixes could be used in multiple renderers.  It is probably best
	 *    to iterate the matrixes and then loop through the renderers
	 *    associated with the matrix at each data point.  This allows us to
	 *    cooperatively move things forward.  It also allows us to make a short
	 *    private list for the renderers involved instead of iterating the
	 *    large list many times.
	 */

	/*
	 * Iterate through each of the matrixes and draw their datapionts
	 * with the various renderers.
	 */
	for (mlist = mall; mlist; mlist = mlist->next) {
		PkgGraphAttribute *attr;
		PkgMatrixIter iter;
		GList *rall = NULL;

		/*
		 * Build a list of renderers/attributes involved.
		 */
		for (alist = aall; alist; alist= alist->next) {
			attr = alist->data;

			if (attr->matrix == mlist->data) {
				rall = g_list_prepend(rall, attr);
			}
		}

		/*
		 * Initialize the matrix iter.
		 */
		pkg_matrix_iter_init(&iter, mlist->data);

		/*
		 * Loop through the matrix data points.
		 */
		while (pkg_matrix_iter_next(&iter)) {
			/*
			 * Apply the various attributes to the particular renderers.
			 */
			for (alist = rall; alist; alist = alist->next) {
				attr = alist->data;
				g_object_set(attr->renderer,
				             attr->attribute,
				             pkg_matrix_iter_get(&iter, attr->column),
				             NULL);
			}

			/*
			 * Render the next value in the renderer.
			 *
			 * XXX: Don't duplicate renderings.
			 */
			for (alist = rall; alist; alist = alist->next) {
				attr = alist->data;
				pkg_renderer_render(attr->renderer);
			}
		}

		/*
		 * Cleanup our short list.
		 */
		g_list_free(rall);
	}
}

static void
pkg_graph_2d_paint (PkgGraph2d *graph)
{
	PkgGraph2dPrivate *priv = graph->priv;
	cairo_t *cr;
	gfloat w, h;
	gdouble x0, x1, x2;
	gdouble y0, y1, y2;
	gdouble dashes[] = {1., 2.};
	gdouble *epochs = NULL;
	gdouble tmp;
	gsize n_epochs = 0;
	gint i;

	clutter_cairo_texture_clear(CLUTTER_CAIRO_TEXTURE(priv->texture));
	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);

	/*
	 * Create cairo context and set defaults.
	 */
	cr = clutter_cairo_texture_create(CLUTTER_CAIRO_TEXTURE(priv->texture));
	cairo_set_line_width(cr, 1.);
	cairo_set_dash(cr, dashes, 2, 0.);

	/*
	 * Get coordinates for drawing primary extends.
	 */
	pkg_graph_2d_get_primary_extents(graph, &x0, &x1, &x2, &y0, &y1, &y2);

	/*
	 * Draw the background color.
	 */
	cairo_set_source_rgb(cr, 1., 1., 1.);
	cairo_rectangle(cr, x1, y2, x2 - x1, y1);
	cairo_fill(cr);

	/*
	 * Draw the bounding box.
	 */
	cairo_set_source_rgb(cr, 0., 0., 0.);
	cairo_move_to(cr, x0, y2);
	cairo_line_to(cr, x2, y2);
	cairo_line_to(cr, x2, y0);
	cairo_move_to(cr, x0, y1);
	cairo_line_to(cr, x2, y1);
	cairo_move_to(cr, x1, y0 - .5);
	cairo_line_to(cr, x1, y2);
	cairo_stroke(cr);

	/*
	 * Draw the x-scale epochs.
	 */
	if (pkg_scale_get_epochs(priv->x_scale, &epochs, &n_epochs)) {
		if (n_epochs > 0) {
			for (i = 0; i < n_epochs; i++) {
				if (pkg_scale_translate(priv->x_scale, epochs[i], &tmp)) {
					PangoFontDescription *font_desc;
					PangoLayout *layout;
					gchar *format, *markup;
					gint pw, ph;

					/*
					 * Only draw the line if it isn't right near the end of
					 * the visiable region of the graph.
					 */
					if (tmp + 5. < floor(w)) {
						cairo_move_to(cr, tmp, y0 - .5);
						cairo_line_to(cr, tmp, y2);
					}

					/*
					 * Draw the y-scale grid-line value.
					 */
					format = pkg_scale_format_string(priv->x_scale, epochs[i]);
					if (format != NULL) {
						font_desc = pango_font_description_from_string("Bold Sans 8");
						layout = pango_cairo_create_layout(cr);
						pango_layout_set_font_description(layout, font_desc);
						markup = g_strdup_printf("<span size=\"smaller\">%s</span>", format);
						pango_layout_set_markup(layout, markup, -1);
						pango_layout_get_pixel_size(layout, &pw, &ph);
						if (i == 0) {
							cairo_move_to(cr, tmp + 2., y1 + 2.);
						} else {
							cairo_move_to(cr, tmp - pw - 2., y1 + 2.);
						}
						pango_cairo_show_layout(cr, layout);
						g_object_unref(layout);
						pango_font_description_free(font_desc);
						g_free(format);
						g_free(markup);
					}
				}
			}
			cairo_stroke(cr);
		}
	}

	/*
	 * Draw the y-scale epochs.
	 */
	if (pkg_scale_get_epochs(priv->y_scale, &epochs, &n_epochs)) {
		if (n_epochs > 0) {
			for (i = 0; i < n_epochs; i++) {
				if (pkg_scale_translate(priv->y_scale, epochs[i], &tmp)) {
					PangoFontDescription *font_desc;
					PangoLayout *layout;
					gchar *format, *markup;
					gint pw, ph;

					cairo_move_to(cr, x0, floor(y1 - tmp) + .5);
					cairo_line_to(cr, x2, floor(y1 - tmp) + .5);

					/*
					 * Draw the y-scale grid-line value.
					 */
					format = pkg_scale_format_string(priv->y_scale, epochs[i]);
					if (format != NULL) {
						font_desc = pango_font_description_from_string("Bold Sans 8");
						layout = pango_cairo_create_layout(cr);
						pango_layout_set_font_description(layout, font_desc);
						markup = g_strdup_printf("<span size=\"smaller\">%s</span>", format);
						pango_layout_set_markup(layout, markup, -1);
						pango_layout_get_pixel_size(layout, &pw, &ph);
						if (i == 0) {
							cairo_move_to(cr, x1 - pw - 2., y1 - tmp - ph - 2.);
						} else {
							cairo_move_to(cr, x1 - pw - 2., y1 - tmp + 2.);
						}
						pango_cairo_show_layout(cr, layout);
						g_object_unref(layout);
						pango_font_description_free(font_desc);
						g_free(markup);
						g_free(format);
					}
				}
			}
			cairo_stroke(cr);
		}
	}

	priv->x_label = "X Label Here";
	priv->y_label = "Y Label Here";

	/*
	 * Draw the X scale label.
	 */
	if (priv->x_label) {
		PangoFontDescription *font_desc;
		PangoLayout *layout;
		gchar *markup;
		gint pw, ph;

		font_desc = pango_font_description_from_string("Bold Sans 10");
		layout = pango_cairo_create_layout(cr);
		pango_layout_set_font_description(layout, font_desc);
		markup = g_strdup_printf("<span size=\"smaller\">%s</span>",
		                         priv->x_label);
		pango_layout_set_markup(layout, markup, -1);
		pango_layout_get_pixel_size(layout, &pw, &ph);
		cairo_move_to(cr, ((w - pw) / 2.), h - ph);
		pango_cairo_show_layout(cr, layout);
		g_object_unref(layout);
		pango_font_description_free(font_desc);
		g_free(markup);
	}

	/*
	 * Draw the Y scale label.
	 */
	if (priv->x_label) {
		PangoFontDescription *font_desc;
		PangoLayout *layout;
		gchar *markup;
		gint pw, ph;

		font_desc = pango_font_description_from_string("Bold Sans 10");
		layout = pango_cairo_create_layout(cr);
		pango_layout_set_font_description(layout, font_desc);
		markup = g_strdup_printf("<span size=\"smaller\">%s</span>",
		                         priv->y_label);
		pango_layout_set_markup(layout, markup, -1);
		pango_layout_get_pixel_size(layout, &pw, &ph);
		cairo_move_to(cr, 0., h - ((h - pw) / 2.));
		cairo_rotate(cr, M_PI / -2.);
		pango_cairo_show_layout(cr, layout);
		g_object_unref(layout);
		pango_font_description_free(font_desc);
		g_free(markup);
	}

	/*
	 * Free cairo context and resources.
	 */
	cairo_destroy(cr);

	/*
	 * Render the children data points.
	 */
	pkg_graph_2d_paint_children(graph);
}

/**
 * pkg_graph_2d_get_x_scale:
 * @graph: A #PkgGraph2d.
 *
 * Retrieves the #PkgScale for the X axis.
 *
 * Returns: A #PkgScale
 * Side effects: None.
 */
PkgScale*
pkg_graph_2d_get_x_scale (PkgGraph2d *graph)
{
	g_return_val_if_fail(PKG_IS_GRAPH_2D(graph), NULL);
	return graph->priv->x_scale;
}

/**
 * pkg_graph_2d_get_y_scale:
 * @graph: A #PkgGraph2d.
 *
 * Retrieves the #PkgScale for the Y axis.
 *
 * Returns: A #PkgScale.
 * Side effects: None.
 */
PkgScale*
pkg_graph_2d_get_y_scale (PkgGraph2d *graph)
{
	g_return_val_if_fail(PKG_IS_GRAPH_2D(graph), NULL);
	return graph->priv->y_scale;
}

/**
 * pkg_graph_2d_set_x_scale:
 * @graph: A #PkgGraph.
 * @scale: A #PkgScale.
 *
 * Sets the #PkgScale for the X axis of the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_2d_set_x_scale (PkgGraph2d *graph,
                          PkgScale   *scale)
{
	PkgGraph2dPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH_2D(graph));
	g_return_if_fail(!scale || PKG_IS_SCALE(scale));

	priv = graph->priv;

	if (!scale) {
		scale = pkg_scale_linear_new();
	}

	g_object_unref(priv->x_scale);
	priv->x_scale = g_object_ref_sink(scale);
	pkg_graph_2d_paint(graph);
	clutter_actor_queue_redraw(CLUTTER_ACTOR(graph));
}

/**
 * pkg_graph_2d_set_y_scale:
 * @scale: A #PkgGraph2d.
 *
 * Sets the #PkgScale for the Y axis of the graph.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_graph_2d_set_y_scale (PkgGraph2d *graph,
                          PkgScale   *scale)
{
	PkgGraph2dPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH_2D(graph));
	g_return_if_fail(!scale || PKG_IS_SCALE(scale));

	priv = graph->priv;

	if (!scale) {
		scale = pkg_scale_linear_new();
	}

	g_object_unref(priv->y_scale);
	priv->y_scale = g_object_ref_sink(scale);
	pkg_graph_2d_paint(graph);
	clutter_actor_queue_redraw(CLUTTER_ACTOR(graph));
}

static void
pkg_graph_2d_place_child (PkgGraph2d  *graph,
                          PkgRenderer *renderer)
{
	PkgGraph2dPrivate *priv = graph->priv;
	gfloat w, h;

	g_debug("renderer place child %p", renderer);

	clutter_actor_get_size(CLUTTER_ACTOR(graph), &w, &h);
	clutter_actor_set_size(CLUTTER_ACTOR(renderer),
	                       w - priv->x_padding,
	                       h - priv->y_padding);
	clutter_actor_set_position(CLUTTER_ACTOR(renderer),
	                           priv->x_padding,
	                           priv->y_padding);
	pkg_graph_2d_paint(graph);
	clutter_actor_queue_redraw(CLUTTER_ACTOR(graph));
}

static void
pkg_graph_2d_allocation_changed (PkgGraph2d             *graph,
                                 const ClutterActorBox  *box,
                                 ClutterAllocationFlags  flags)
{
	PkgGraph2dPrivate *priv = graph->priv;
	gdouble x0, x1, x2;
	gdouble y0, y1, y2;

	clutter_actor_set_size(priv->texture,
	                       clutter_actor_box_get_width(box),
	                       clutter_actor_box_get_height(box));
	clutter_cairo_texture_set_surface_size(CLUTTER_CAIRO_TEXTURE(priv->texture),
	                                       clutter_actor_box_get_width(box),
	                                       clutter_actor_box_get_height(box));

	if (priv->renderer) {
		pkg_graph_2d_place_child(graph, PKG_RENDERER(priv->renderer));
		pkg_renderer_clear(PKG_RENDERER(priv->renderer));
	}

	pkg_graph_2d_get_primary_extents(graph, &x0, &x1, &x2, &y0, &y1, &y2);
	pkg_scale_set_pixel_range(priv->x_scale, x1, x2);
	pkg_scale_set_pixel_range(priv->y_scale, y2, y1);
	pkg_graph_2d_paint(graph);
}

static void
pkg_graph_2d_real_pack_renderer (PkgGraph    *graph,
                                 PkgRenderer *renderer)
{
	PkgGraph2dPrivate *priv;

	g_return_if_fail(PKG_IS_GRAPH_2D(graph));
	g_return_if_fail(PKG_IS_RENDERER(renderer));

	priv = PKG_GRAPH_2D(graph)->priv;

	if (priv->renderer) {
		g_warning("Renderer already added. "
		          "No support for multiple renderers.");
		return;
	}

	priv->renderer = g_object_ref_sink(renderer);
	clutter_container_add_actor(CLUTTER_CONTAINER(graph),
	                            CLUTTER_ACTOR(renderer));
	pkg_graph_2d_place_child(PKG_GRAPH_2D(graph), renderer);
}

static void
pkg_graph_2d_real_render (PkgGraph *graph)
{
}

static void
pkg_graph_2d_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_graph_2d_parent_class)->finalize(object);
}

static void
pkg_graph_2d_class_init (PkgGraph2dClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_graph_2d_finalize;
	g_type_class_add_private(object_class, sizeof(PkgGraph2dPrivate));
}

static void
pkg_graph_2d_init (PkgGraph2d *graph)
{
	graph->priv = G_TYPE_INSTANCE_GET_PRIVATE(graph,
	                                          PKG_TYPE_GRAPH_2D,
	                                          PkgGraph2dPrivate);

	graph->priv->texture = clutter_cairo_texture_new(1, 1);
	clutter_container_add_actor(CLUTTER_CONTAINER(graph),
	                            graph->priv->texture);
	clutter_actor_show(graph->priv->texture);

	graph->priv->x_scale = g_object_ref_sink(pkg_scale_linear_new());
	graph->priv->y_scale = g_object_ref_sink(pkg_scale_linear_new());
	graph->priv->x_padding = 25.;
	graph->priv->y_padding = 25.;

	g_signal_connect(graph,
	                 "allocation-changed",
	                 G_CALLBACK(pkg_graph_2d_allocation_changed),
	                 NULL);
}

static void
pkg_graph_init (PkgGraphIface *iface)
{
	iface->pack_renderer = pkg_graph_2d_real_pack_renderer;
	iface->render = pkg_graph_2d_real_render;
}
