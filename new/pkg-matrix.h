/* pkg-matrix.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_MATRIX_H__
#define __PKG_MATRIX_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define PKG_TYPE_MATRIX            (pkg_matrix_get_type())
#define PKG_MATRIX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_MATRIX, PkgMatrix))
#define PKG_MATRIX_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_MATRIX, PkgMatrix const))
#define PKG_MATRIX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_MATRIX, PkgMatrixClass))
#define PKG_IS_MATRIX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_MATRIX))
#define PKG_IS_MATRIX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_MATRIX))
#define PKG_MATRIX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_MATRIX, PkgMatrixClass))

typedef struct _PkgMatrix        PkgMatrix;
typedef struct _PkgMatrixClass   PkgMatrixClass;
typedef struct _PkgMatrixPrivate PkgMatrixPrivate;
typedef struct _PkgMatrixIter    PkgMatrixIter;

struct _PkgMatrix
{
	GObject parent;

	/*< private >*/
	PkgMatrixPrivate *priv;
};

struct _PkgMatrixClass
{
	GObjectClass parent_class;
};

struct _PkgMatrixIter
{
	PkgMatrix *matrix;
	gsize      begin;
	gsize      offset;
};

GType      pkg_matrix_get_type  (void) G_GNUC_CONST;
PkgMatrix* pkg_matrix_new       (gsize          rows,
                                 gint           columns);
gdouble    pkg_matrix_get       (PkgMatrix     *matrix,
                                 gsize          row,
                                 gint           column);
void       pkg_matrix_set       (PkgMatrix     *matrix,
                                 gsize          row,
                                 gint           column,
                                 gdouble        value);
void       pkg_matrix_set_row   (PkgMatrix     *matrix,
                                 gsize          row,
                                 gdouble       *values);
void       pkg_matrix_iter_init (PkgMatrixIter *iter,
                                 PkgMatrix     *matrix);
gboolean   pkg_matrix_iter_next (PkgMatrixIter *iter);
gdouble    pkg_matrix_iter_get  (PkgMatrixIter *iter,
                                 gint           column);

G_END_DECLS

#endif /* __PKG_MATRIX_H__ */
