/* pkg-matrix.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <string.h>

#include "pkg-matrix.h"

/**
 * SECTION:pkg-matrix
 * @title: PkgMatrix
 * @short_description: 
 *
 * Section overview.
 */

G_DEFINE_TYPE(PkgMatrix, pkg_matrix, G_TYPE_OBJECT)

struct _PkgMatrixPrivate
{
	GArray *array;     /* array of matrix values */
	gsize   begin;     /* current offset in circular buffer */
	gsize   n_rows;    /* total number of rows in circular buffer */
	gint    n_columns; /* total number of columns in circular buffer */
};

/**
 * pkg_matrix_new:
 *
 * Creates a new instance of #PkgMatrix.
 *
 * Returns: the newly created instance of #PkgMatrix.
 * Side effects: None.
 */
PkgMatrix*
pkg_matrix_new (gsize rows,
                gint  columns)
{
	PkgMatrix *matrix;
	
	matrix = g_object_new(PKG_TYPE_MATRIX, NULL);
	matrix->priv->n_rows = rows;
	matrix->priv->n_columns = columns;
	matrix->priv->array = g_array_sized_new(FALSE,
	                                        TRUE,
	                                        sizeof(gdouble),
	                                        rows * columns);

	return matrix;
}

/**
 * pkg_matrix_set:
 * @matrix: A #PkgMatrix.
 * @row: The matrix row
 * @column: The column within the row
 * @value: The value to set.
 *
 * Sets a value within the matrix.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_matrix_set (PkgMatrix *matrix,
                gsize      row,
                gint       column,
                gdouble    value)
{
	gdouble *ptr;

	g_return_if_fail(matrix != NULL);
	g_return_if_fail(row >= 0 && row < matrix->priv->n_rows);
	g_return_if_fail(column >= 0 && column < matrix->priv->n_columns);

	ptr = &g_array_index(matrix->priv->array, gdouble,
	                     (row * matrix->priv->n_columns) + column);
	*ptr = value;
}

/**
 * pkg_matrix_get:
 * @matrix: A #PkgMatrix.
 *
 * Retrieves a value from the #PkgMatrix.
 *
 * Returns: A #gdouble containing the value.
 * Side effects: None.
 */
gdouble
pkg_matrix_get (PkgMatrix *matrix,
                gsize      row,
                gint       column)
{
	g_return_val_if_fail(matrix != NULL, NAN);
	g_return_val_if_fail(row >= 0 && row < matrix->priv->n_rows, NAN);
	g_return_val_if_fail(column >= 0 && column < matrix->priv->n_columns, NAN);
	g_return_val_if_fail(matrix->priv->array != NULL, NAN);

	return g_array_index(matrix->priv->array, gdouble,
	                     (row * matrix->priv->n_columns) + column);
}

/**
 * pkg_matrix_set_row:
 * @matrix: A #PkgMatrix.
 *
 * Sets the values for a particular row within the matrix.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_matrix_set_row (PkgMatrix *matrix,
                    gsize      row,
                    gdouble   *values)
{
	gdouble *ptr;

	g_return_if_fail(matrix != NULL);
	g_return_if_fail(row >= 0 && row < matrix->priv->n_rows);

	ptr = &g_array_index(matrix->priv->array, gdouble,
	                     (row * matrix->priv->n_columns));
	memcpy(ptr, values, sizeof(gdouble) * matrix->priv->n_columns);
}

/**
 * pkg_matrix_iter_init:
 * @iter: A #PkgMatrixIter.
 *
 * Initializes a #PkgMatrixIter for iterating through tuples within
 * the matrix.
 *
 * See pkg_matrix_iter_next().
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_matrix_iter_init (PkgMatrixIter *iter,
                      PkgMatrix     *matrix)
{
	g_return_if_fail(PKG_IS_MATRIX(matrix));
	g_return_if_fail(iter != NULL);

	iter->matrix = matrix;
	iter->begin = matrix->priv->begin;
	iter->offset = -1;
}

/**
 * pkg_matrix_iter_next:
 * @iter: A #PkgMatrixIter.
 *
 * Increments the position of the #PkgMatrixIter to point at the next position
 * within the matrix.
 *
 * See pkg_matrix_iter_get().
 *
 * Returns: None.
 * Side effects: None.
 */
gboolean
pkg_matrix_iter_next (PkgMatrixIter *iter)
{
	g_return_val_if_fail(iter != NULL, FALSE);
	g_return_val_if_fail(iter->matrix != NULL, FALSE);

	iter->offset++;
	return (iter->offset < iter->matrix->priv->n_rows);
}

/**
 * pkg_matrix_iter_get:
 * @iter: A #PkgMatrixIter.
 * @column: The desired column.
 *
 * Retreives the value in @column of the row pointed to by @iter.
 *
 * Returns: the value found in the selected row and column.
 * Side effects: None.
 */
gdouble
pkg_matrix_iter_get (PkgMatrixIter *iter,
                     gint           column)
{
	PkgMatrixPrivate *priv;

	g_return_val_if_fail(iter != NULL, NAN);
	g_return_val_if_fail(iter->matrix != NULL, NAN);
	g_return_val_if_fail(column >= 0, NAN);

	priv = iter->matrix->priv;

	return pkg_matrix_get(iter->matrix,
	                      ((iter->begin + iter->offset) % priv->n_rows),
	                      column);
}

static void
pkg_matrix_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_matrix_parent_class)->finalize(object);
}

static void
pkg_matrix_class_init (PkgMatrixClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_matrix_finalize;
	g_type_class_add_private(object_class, sizeof(PkgMatrixPrivate));
}

static void
pkg_matrix_init (PkgMatrix *matrix)
{
	matrix->priv = G_TYPE_INSTANCE_GET_PRIVATE(matrix,
	                                           PKG_TYPE_MATRIX,
	                                           PkgMatrixPrivate);
}
