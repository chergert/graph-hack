#include <stdlib.h>
#include <clutter/clutter.h>
#include <glib.h>

#include "pkg-graph-2d.h"
#include "pkg-renderer-line.h"

#define WIDTH 640
#define HEIGHT 600
#define PADDING 10
#define IN_WIDTH (WIDTH - (PADDING * 2))
#define IN_HEIGHT (HEIGHT - (PADDING * 2))

/*
static gchar*
format_func (PkgScale *scale,
             gdouble   value,
             gpointer  user_data)
{
	return g_strdup_printf("%.1f", value);
}
*/

gint
main (gint   argc,
      gchar *argv[])
{
	ClutterActor *stage, *graph, *renderer, *group;
	ClutterColor  color = {240, 235, 226, 255};
	PkgScale *scale;
	PkgMatrix *matrix;
	gint i;

	clutter_init(&argc, &argv);

	stage = clutter_stage_get_default();
	clutter_stage_set_title(CLUTTER_STAGE(stage), "Graph Hack");
	clutter_stage_set_color(CLUTTER_STAGE(stage), &color);
	clutter_actor_set_size(stage, WIDTH, HEIGHT);
	clutter_actor_show(stage);

	group = clutter_group_new();
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), group);
	clutter_actor_set_size(group, IN_WIDTH, IN_HEIGHT);
	clutter_actor_set_position(group, PADDING, PADDING);
	clutter_actor_show(group);

	graph = pkg_graph_2d_new();
	clutter_container_add_actor(CLUTTER_CONTAINER(group), graph);
	clutter_actor_set_size(graph, IN_WIDTH, IN_HEIGHT);
	clutter_actor_show(graph);

	renderer = pkg_renderer_line_new();
	pkg_graph_pack_renderer(PKG_GRAPH(graph), PKG_RENDERER(renderer));
	clutter_actor_show(renderer);

	/*
	 * Tmp, setup custom scale.
	 */
	scale = pkg_graph_2d_get_x_scale(PKG_GRAPH_2D(graph));
	pkg_scale_set_range(scale, 0, 200);
	pkg_renderer_line_set_x_scale(PKG_RENDERER_LINE(renderer), scale);
	//pkg_scale_set_format_func(scale, format_func, NULL, NULL);
	scale = pkg_graph_2d_get_y_scale(PKG_GRAPH_2D(graph));
	//pkg_scale_set_format_func(scale, format_func, NULL, NULL);
	pkg_scale_set_range(scale, 0, 200);
	pkg_renderer_line_set_y_scale(PKG_RENDERER_LINE(renderer), scale);

	/*
	 * Build the matrix of data by hand for testing.
	 */
	matrix = pkg_matrix_new(10, 2);
	for (i = 0; i < 10; i++) {
		pkg_matrix_set(matrix, i, 0, i*i);
		pkg_matrix_set(matrix, i, 1, i*i);
	}

	/*
	 * Attach the matrix and set a few attributes.
	 */
	g_debug("Attaching matrix");
	pkg_graph_add_matrix(PKG_GRAPH(graph), matrix);
	pkg_graph_add_attribute(PKG_GRAPH(graph), PKG_RENDERER(renderer), matrix, "x-data", 0);
	pkg_graph_add_attribute(PKG_GRAPH(graph), PKG_RENDERER(renderer), matrix, "y-data", 1);
	g_object_unref(matrix);

	clutter_main();

	return EXIT_SUCCESS;
}
