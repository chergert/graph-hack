/* pkg-scale.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-scale.h"

static GQuark pkg_scale_format_func_quark   (void) G_GNUC_CONST;
static void   pkg_scale_format_func_destroy (gpointer user_data);

typedef struct
{
	PkgScaleFormatFunc callback;
	gpointer           user_data;
	GDestroyNotify     destroy;
} PkgScaleFormatFuncData;

/**
 * pkg_scale_set_format_func:
 * @scale: A #PkgScale.
 *
 * Sets a function callback to be used to format a particular value
 * as a string.  This method will be called by pkg_scale_format_string().
 *
 * Returns: None>
 * Side effects: None.
 */
void
pkg_scale_set_format_func (PkgScale           *scale,
                           PkgScaleFormatFunc  format_func,
                           gpointer            user_data,
                           GDestroyNotify      destroy)
{
	PkgScaleFormatFuncData *data;

	g_return_if_fail(PKG_IS_SCALE(scale));
	g_return_if_fail(format_func != NULL);

	data = g_slice_new0(PkgScaleFormatFuncData);
	data->callback = format_func;
	data->user_data = user_data;
	data->destroy = destroy;

	g_object_set_qdata_full(G_OBJECT(scale),
	                        pkg_scale_format_func_quark(),
	                        data,
	                        pkg_scale_format_func_destroy);
}

/**
 * pkg_scale_format_string:
 * @scale: A #PkgScale.
 * @value: A #gdouble to format.
 *
 * Formats a value as a string to be used on a graph.
 *
 * Returns: A newly allocated string which should be freed with g_free().
 * Side effects: None.
 */
gchar*
pkg_scale_format_string (PkgScale *scale,
                         gdouble   value)
{
	PkgScaleFormatFuncData *data;

	g_return_val_if_fail(PKG_IS_SCALE(scale), NULL);

	data = g_object_get_qdata(G_OBJECT(scale), pkg_scale_format_func_quark());

	if (data != NULL) {
		return data->callback(scale, value, data->user_data);
	} else if (!PKG_SCALE_GET_INTERFACE(scale)->format_string) {
		return g_strdup_printf("%.0f", value);
	} else {
		return PKG_SCALE_GET_INTERFACE(scale)->format_string(scale, value);
	}
}

/**
 * pkg_scale_get_epochs:
 * @graph: A #PkgGraph.
 * @epochs: A location for an array of #gdouble.
 * @n_epochs: The number of elements in @epochs.
 *
 * Retrieves the important epochs within a graph.  This is typically used
 * for specifying where the inner grids are placed within a graph.
 *
 * @epochs is allocated with g_malloc() and should be freed by the caller
 * using g_free().
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 * Side effects: None.
 */
gboolean
pkg_scale_get_epochs (PkgScale  *scale,
                      gdouble  **epochs,
                      gsize     *n_epochs)
{
	g_return_val_if_fail(PKG_IS_SCALE(scale), FALSE);
	g_return_val_if_fail(epochs != NULL, FALSE);

	if (!PKG_SCALE_GET_INTERFACE(scale)->get_epochs) {
		return FALSE;
	}

	return PKG_SCALE_GET_INTERFACE(scale)->get_epochs(scale, epochs, n_epochs);
}

/**
 * pkg_scale_set_range:
 * @scale: A #PkgScale.
 *
 * Sets the range for which the scale should be translating coordinates.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_scale_set_range (PkgScale *scale,
                     gdouble   begin,
                     gdouble   end)
{
	g_return_if_fail(PKG_IS_SCALE(scale));
	PKG_SCALE_GET_INTERFACE(scale)->set_range(scale, begin, end);
}

/**
 * pkg_scale_set_pixel_range:
 * @scale: A #PkgScale.
 *
 * Sets the range of pixels for which the scale should be translating
 * coordinates.
 *
 * Returns: None.
 * Side effects: None.
 */
void
pkg_scale_set_pixel_range (PkgScale *scale,
                           gdouble   begin,
                           gdouble   end)
{
	g_return_if_fail(PKG_IS_SCALE(scale));
	PKG_SCALE_GET_INTERFACE(scale)->set_pixel_range(scale, begin, end);
}

/**
 * pkg_scale_translate:
 * @scale: A #PkgScale.
 * @in_coord: The raw input coordinate.
 * @out_coord: The translated output coordinate for the graph.
 *
 * Translates a double into coordinate position within a scale.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 * Side effects: None.
 */
gboolean
pkg_scale_translate (PkgScale *scale,
                     gdouble   in_coord,
                     gdouble  *out_coord)
{
	g_return_val_if_fail(PKG_IS_SCALE(scale), FALSE);
	return PKG_SCALE_GET_INTERFACE(scale)->
		translate(scale, in_coord, out_coord);
}

GType
pkg_scale_get_type (void)
{
	static GType type_id = 0;

	if (g_once_init_enter((gsize *)&type_id)) {
		GType _type_id;
		const GTypeInfo g_type_info = {
			sizeof(PkgScaleIface),
			NULL, /* base_init */
			NULL, /* base_finalize */
			NULL, /* class_init */
			NULL, /* class_finalize */
			NULL, /* class_data */
			0,    /* instance_size */
			0,    /* n_preallocs */
			NULL, /* instance_init */
			NULL  /* value_vtable */
		};

		_type_id = g_type_register_static(G_TYPE_INTERFACE,
		                                  "PkgScale",
		                                  &g_type_info,
		                                  0);
		g_type_interface_add_prerequisite(_type_id, G_TYPE_OBJECT);
		g_once_init_leave((gsize *)&type_id, _type_id);
	}

	return type_id;
}

static GQuark
pkg_scale_format_func_quark (void)
{
	return g_quark_from_static_string("pkg-scale-format-func");
}

static void
pkg_scale_format_func_destroy (gpointer user_data)
{
	PkgScaleFormatFuncData *data = user_data;
	g_assert(data);

	if (G_LIKELY(data->destroy)) {
		data->destroy(data->user_data);
	}

	g_slice_free(PkgScaleFormatFuncData, data);
}
