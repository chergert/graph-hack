/* pkg-graph.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_GRAPH_H__
#define __PKG_GRAPH_H__

#include <clutter/clutter.h>

#include "pkg-matrix.h"
#include "pkg-renderer.h"

G_BEGIN_DECLS

#define PKG_TYPE_GRAPH             (pkg_graph_get_type())
#define PKG_GRAPH(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    PKG_TYPE_GRAPH, PkgGraph))
#define PKG_IS_GRAPH(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    PKG_TYPE_GRAPH))
#define PKG_GRAPH_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), PKG_TYPE_GRAPH, PkgGraphIface))

typedef struct _PkgGraph          PkgGraph;
typedef struct _PkgGraphIface     PkgGraphIface;
typedef struct _PkgGraphAttribute PkgGraphAttribute;

struct _PkgGraphAttribute
{
	PkgMatrix   *matrix;
	PkgRenderer *renderer;
	gchar       *attribute;
	gint         column;
};

struct _PkgGraphIface
{
	GTypeInterface parent;

	void (*pack_renderer)  (PkgGraph    *graph,
	                        PkgRenderer *renderer);
	void (*render)         (PkgGraph    *graph);
};

GType  pkg_graph_get_type       (void) G_GNUC_CONST;
void   pkg_graph_pack_renderer  (PkgGraph    *graph,
                                 PkgRenderer *renderer);
void   pkg_graph_add_matrix     (PkgGraph    *graph,
                                 PkgMatrix   *matrix);
void   pkg_graph_add_attribute  (PkgGraph    *graph,
                                 PkgRenderer *renderer,
                                 PkgMatrix   *matrix,
                                 const gchar *attribute,
                                 gint         column);
void   pkg_graph_render         (PkgGraph    *graph);
GList* pkg_graph_get_attributes (PkgGraph    *graph);
GList* pkg_graph_get_matrixes   (PkgGraph    *graph);

G_END_DECLS

#endif /* __PKG_GRAPH_H__ */
