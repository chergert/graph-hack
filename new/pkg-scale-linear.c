/* pkg-scale-linear.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "pkg-scale-linear.h"

/**
 * SECTION:pkg-scale-linear
 * @title: PkgScaleLinear
 * @short_description: 
 *
 * Section overview.
 */

#define UPDATE_RATIO(p) G_STMT_START {              \
    (p)->ratio = ABS((p)->pix_end - (p)->pix_begin) \
               / ABS((p)->end - (p)->begin);        \
} G_STMT_END

static void pkg_scale_init (PkgScaleIface *iface);

G_DEFINE_TYPE_EXTENDED(PkgScaleLinear,
                       pkg_scale_linear,
                       G_TYPE_INITIALLY_UNOWNED,
                       0,
                       G_IMPLEMENT_INTERFACE(PKG_TYPE_SCALE, pkg_scale_init))

struct _PkgScaleLinearPrivate
{
	gdouble begin;
	gdouble end;
	gdouble pix_begin;
	gdouble pix_end;
	gdouble ratio;
	gint    n_ticks;
};

/**
 * pkg_scale_linear_new:
 *
 * Creates a new instance of #PkgScaleLinear.
 *
 * Returns: the newly created instance of #PkgScaleLinear.
 * Side effects: None.
 */
PkgScale*
pkg_scale_linear_new (void)
{
	return g_object_new(PKG_TYPE_SCALE_LINEAR, NULL);
}

gdouble
pkg_scale_linear_nicenum (gdouble  x,
                          gboolean round)
{
	gint    exp; /* exponent of x */
	gdouble f;   /* fractional part of x */
	gdouble nf;  /* nice, rounded fraction */

	exp = floor(log10(x));
	f = x / pow(10., exp);

	if (round) {
		if (f < 1.5)
			nf = 1.;
		else if (f < 3.)
			nf = 2.;
		else if (f < 7.)
			nf = 5.;
		else
			nf = 10.;
	} else {
		if (f <= 1.)
			nf = 1.;
		else if (f <= 2.)
			nf = 2.;
		else if (f <= 5.)
			nf = 5.;
		else
			nf = 10.;
	}

	return nf * pow(10., exp);
}

static gboolean
pkg_scale_linear_real_get_epochs (PkgScale  *scale,
                                  gdouble  **epochs,
                                  gsize     *n_epochs)
{
	PkgScaleLinearPrivate *priv = PKG_SCALE_LINEAR(scale)->priv;
	gdouble *vals;
	gdouble  d;
	gdouble  graphmin;
	gdouble  graphmax;
	gdouble  range;
	gdouble  x;
	gint     nfrac;
	gint     i;

	vals = g_malloc(sizeof(gdouble) * priv->n_ticks * 2);

	range = pkg_scale_linear_nicenum(priv->end - priv->begin, FALSE);
	d = pkg_scale_linear_nicenum(range / (priv->n_ticks - 1), TRUE);
	graphmin = floor(priv->begin / d) * d;
	graphmax = ceil(priv->end / d) * d;
	nfrac = MAX(-floor(log10(d)), 0);

	for (x = graphmin, i = 0; x <= graphmax; x += (.5 * d), i++) {
		g_assert_cmpint(i, <, priv->n_ticks * 2);
		vals[i] = x;
	}

	*epochs = vals;
	*n_epochs = i;

	return TRUE;
}

static void
pkg_scale_linear_real_set_range (PkgScale *scale,
                                 gdouble   begin,
                                 gdouble   end)
{
	PkgScaleLinearPrivate *priv;

	g_return_if_fail(PKG_IS_SCALE_LINEAR(scale));

	priv = PKG_SCALE_LINEAR(scale)->priv;

	priv->begin = begin;
	priv->end = end;
	UPDATE_RATIO(priv);
}

static void
pkg_scale_linear_real_set_pixel_range (PkgScale *scale,
                                       gdouble   begin,
                                       gdouble   end)
{
	PkgScaleLinearPrivate *priv;

	g_return_if_fail(PKG_IS_SCALE_LINEAR(scale));

	priv = PKG_SCALE_LINEAR(scale)->priv;

	priv->pix_begin = begin;
	priv->pix_end = end;
	UPDATE_RATIO(priv);
}

static gboolean
pkg_scale_linear_real_translate (PkgScale *scale,
                                 gdouble   in_coord,
                                 gdouble  *out_coord)
{
	PkgScaleLinearPrivate *priv = ((PkgScaleLinear *)scale)->priv;

	if (in_coord > priv->end) {
		in_coord = priv->end;
	} else if (in_coord < priv->begin) {
		in_coord = priv->begin;
	}

	*out_coord = floor(priv->pix_begin + (in_coord * priv->ratio)) + .5;

	return TRUE;
}

static void
pkg_scale_linear_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_scale_linear_parent_class)->finalize(object);
}

static void
pkg_scale_linear_class_init (PkgScaleLinearClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_scale_linear_finalize;
	g_type_class_add_private(object_class, sizeof(PkgScaleLinearPrivate));
}

static void
pkg_scale_linear_init (PkgScaleLinear *scale)
{
	scale->priv = G_TYPE_INSTANCE_GET_PRIVATE(scale,
	                                          PKG_TYPE_SCALE_LINEAR,
	                                          PkgScaleLinearPrivate);
	scale->priv->n_ticks = 5;
}

static void
pkg_scale_init (PkgScaleIface *iface)
{
	iface->set_range = pkg_scale_linear_real_set_range;
	iface->set_pixel_range = pkg_scale_linear_real_set_pixel_range;
	iface->translate = pkg_scale_linear_real_translate;
	iface->get_epochs = pkg_scale_linear_real_get_epochs;
}
