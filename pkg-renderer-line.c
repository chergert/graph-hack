/* pkg-renderer-line.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkg-renderer-line.h"

/**
 * SECTION:pkg-renderer-line
 * @title: PkgRendererLine
 * @short_description: 
 *
 * Section overview.
 */

static void pkg_renderer_init (PkgRendererIface *iface);

G_DEFINE_TYPE_EXTENDED(PkgRendererLine,
                       pkg_renderer_line,
                       CLUTTER_TYPE_CAIRO_TEXTURE,
                       0,
                       G_IMPLEMENT_INTERFACE(PKG_TYPE_RENDERER,
                                             pkg_renderer_init))

struct _PkgRendererLinePrivate
{
	gpointer dummy;
};

/**
 * pkg_renderer_line_new:
 *
 * Creates a new instance of #PkgRendererLine.
 *
 * Returns: the newly created instance of #PkgRendererLine.
 * Side effects: None.
 */
ClutterActor*
pkg_renderer_line_new (void)
{
	return g_object_new(PKG_TYPE_RENDERER_LINE, NULL);
}

static void
pkg_renderer_line_paint (ClutterActor *actor)
{
	PkgRendererLinePrivate *priv;
	cairo_t *cr;
	gfloat w, h;

	g_return_if_fail(PKG_IS_RENDERER_LINE(actor));

	priv = PKG_RENDERER_LINE(actor)->priv;

	clutter_actor_get_size(actor, &w, &h);

	cr = clutter_cairo_texture_create(CLUTTER_CAIRO_TEXTURE(actor));
	cairo_set_source_rgb(cr, 0., 0., 1.);
	cairo_move_to(cr, 25.5, h - 25.5);
	cairo_line_to(cr, w, 0.);
	cairo_stroke_preserve(cr);
	cairo_line_to(cr, w, h - 25.5);
	cairo_line_to(cr, 25.5, h - 25.5);
	cairo_set_source_rgba(cr, 0., 0., 1., .5);
	cairo_fill(cr);
	cairo_destroy(cr);
}

static void
pkg_renderer_line_allocation_changed (ClutterActor           *actor,
                                      const ClutterActorBox  *box,
                                      ClutterAllocationFlags  flags)
{
	CLUTTER_ACTOR_CLASS(pkg_renderer_line_parent_class)->allocate(actor, box, flags);
	clutter_cairo_texture_set_surface_size(CLUTTER_CAIRO_TEXTURE(actor),
	                                       clutter_actor_box_get_width(box),
	                                       clutter_actor_box_get_height(box));
	pkg_renderer_line_paint(actor);
}

static void
pkg_renderer_line_finalize (GObject *object)
{
	G_OBJECT_CLASS(pkg_renderer_line_parent_class)->finalize(object);
}

static void
pkg_renderer_line_class_init (PkgRendererLineClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = pkg_renderer_line_finalize;
	g_type_class_add_private(object_class, sizeof(PkgRendererLinePrivate));
}

static void
pkg_renderer_line_init (PkgRendererLine *renderer)
{
	renderer->priv = G_TYPE_INSTANCE_GET_PRIVATE(renderer,
	                                             PKG_TYPE_RENDERER_LINE,
	                                             PkgRendererLinePrivate);

	clutter_actor_set_size(CLUTTER_ACTOR(renderer), 10, 10);

	g_signal_connect(renderer,
	                 "allocation-changed",
	                 G_CALLBACK(pkg_renderer_line_allocation_changed),
	                 NULL);
}

static void
pkg_renderer_init (PkgRendererIface *iface)
{
}
