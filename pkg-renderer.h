/* pkg-renderer.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_RENDERER_H__
#define __PKG_RENDERER_H__

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define PKG_TYPE_RENDERER             (pkg_renderer_get_type())
#define PKG_RENDERER(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    PKG_TYPE_RENDERER, PkgRenderer))
#define PKG_IS_RENDERER(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    PKG_TYPE_RENDERER))
#define PKG_RENDERER_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), PKG_TYPE_RENDERER, PkgRendererIface))

typedef struct _PkgRenderer PkgRenderer;
typedef struct _PkgRendererIface PkgRendererIface;

struct _PkgRendererIface
{
	GTypeInterface parent;
};

GType pkg_renderer_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __PKG_RENDERER_H__ */
