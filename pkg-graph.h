/* pkg-graph.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_GRAPH_H__
#define __PKG_GRAPH_H__

#include <clutter/clutter.h>

#include "pkg-data-set.h"
#include "pkg-renderer.h"
#include "pkg-scale.h"

G_BEGIN_DECLS

#define PKG_TYPE_GRAPH            (pkg_graph_get_type())
#define PKG_GRAPH(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH, PkgGraph))
#define PKG_GRAPH_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), PKG_TYPE_GRAPH, PkgGraph const))
#define PKG_GRAPH_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PKG_TYPE_GRAPH, PkgGraphClass))
#define PKG_IS_GRAPH(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PKG_TYPE_GRAPH))
#define PKG_IS_GRAPH_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PKG_TYPE_GRAPH))
#define PKG_GRAPH_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PKG_TYPE_GRAPH, PkgGraphClass))

typedef struct _PkgGraph        PkgGraph;
typedef struct _PkgGraphClass   PkgGraphClass;
typedef struct _PkgGraphPrivate PkgGraphPrivate;

struct _PkgGraph
{
	ClutterGroup parent;

	/*< private >*/
	PkgGraphPrivate *priv;
};

struct _PkgGraphClass
{
	ClutterGroupClass parent_class;
};

GType         pkg_graph_get_type         (void) G_GNUC_CONST;
ClutterActor* pkg_graph_new              (void);
void          pkg_graph_set_scale        (PkgGraph     *graph,
                                          PkgScale     *scale);
void          pkg_graph_set_renderer     (PkgGraph     *graph,
                                          PkgRenderer  *renderer);
void          pkg_graph_set_padding      (PkgGraph     *graph,
                                          gint          xpad,
                                          gint          ypad);
void          pkg_graph_add_data_set     (PkgGraph     *graph,
                                          PkgDataSet   *data_set);
void          pkg_graph_add_attribute    (PkgGraph     *graph,
                                          PkgDataSet   *data_set,
                                          const gchar  *attribute,
                                          gint          axis);
void          pkg_graph_set_attributes   (PkgGraph     *graph,
                                          PkgDataSet   *data_set,
                                          const gchar  *first_attribute,
                                          ...);
void          pkg_graph_clear_attributes (PkgGraph     *graph,
                                          PkgDataSet   *data_set);

G_END_DECLS

#endif /* __PKG_GRAPH_H__ */
