/* pkg-data-set.h
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined (__PERFKIT_INSIDE__) && !defined (PERFKIT_COMPILATION)
#error "Only <perfkit-gui/perfkit-gui.h> can be included directly."
#endif

#ifndef __PKG_DATA_SET_H__
#define __PKG_DATA_SET_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define PKG_TYPE_DATA_SET             (pkg_data_set_get_type())
#define PKG_DATA_SET(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    PKG_TYPE_DATA_SET, PkgDataSet))
#define PKG_IS_DATA_SET(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    PKG_TYPE_DATA_SET))
#define PKG_DATA_SET_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), PKG_TYPE_DATA_SET, PkgDataSetIface))

typedef struct _PkgDataSet PkgDataSet;
typedef struct _PkgDataSetIface PkgDataSetIface;
typedef struct _PkgDataSetIter PkgDataSetIter;

struct _PkgDataSetIface
{
	GTypeInterface parent;
};

struct _PkgDataSetIter
{
	gpointer user_data;
	gpointer user_data2;
	gpointer user_data3;
};

GType    pkg_data_set_get_type  (void) G_GNUC_CONST;

gboolean pkg_data_set_iter_init          (PkgDataSet *data_set, PkgDataSetIter *iter);
gboolean pkg_data_set_iter_next          (PkgDataSetIter *iter);
guint    pkg_data_set_iter_count_columns (PkgDataSetIter *iter);

G_END_DECLS

#endif /* __PKG_DATA_SET_H__ */
